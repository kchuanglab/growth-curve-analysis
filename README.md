Growth Curve Analysis Pipeline
==============================

This is a software pipeline for processing, analysis, and plotting of growth curve data from Tecan plate reader Excel spreadsheets.

External dependencies
---------------------
None.

Installation
------------
### Downloading ###
If you have git installed, it is recommended that you clone this repository into your directory of choice:
```sh
git clone git@bitbucket.org:kchuanglab/cell-wall-sim.git
```
Alternatively, you can [download](https://bitbucket.org/kchuanglab/growth-curve-analysis/downloads) a compressed copy of the repository. If you do this, however, you will have to re-download the repository to receive updates.
### Initialization ###
For general use of the pipeline, you will need to add the following folders (and their subfolders) to your MATLAB path:
* ext
* PlateData
* PlateSpecification

To run the example scripts, you will need to have the directory of a certain experiment under Examples/ in your MATLAB path. Things shouldn't break if you just add the entire Examples/ directory (and its subdirectories) to your MATLAB path.

For more information, check out the README.md files in the Examples/ directory and subdirectories.

Getting Started
---------------
More to come soon!

## Pipeline Design ##
  * The overall idea is that we want to create a Plate object that exactly describes the layout of the plate.
    * A Plate object is constructed with an array of Sample objects. Each Sample corresponds to a set of wells which are all replicates of the same conditions. Conditions are defined by the user as a properties struct fed into the constructor of each Sample object, but it is in your best interest for all such properties structs to have the same structure and fields. Basic information can be found in PlateSpecification/Plate.m and PlateSpecification/Sample.m, and other files in PlateSpecification/
  * We then run a function that extracts raw data from the Tecan excel spreadsheet and stores it as a set of timeseries (more specifically, a Map of timeseries) where each timeseries corresponds to the OD data (over time) for a given well. Basic information can be found in PlateData/importGrowthData.m
  * We then construct a PlateDataSet object using the extracted timeseries set and the Plate object (along with some general settings). This new object exposes the various ways of processing, aggregating and plotting the data. Basic information can be found in plateData/DataSets/PlateDataSet. I tried to document everything inside PlateData pretty thoroughly, and the files in PlateData/DataSets might be a good place to begin (after reading through the documentation in the files in PlateSpecification).

Contributors
------------
* [Ethan Li](mailto:ethanli@stanford.edu): primary maintainer. Send praise/comments/suggestions/bug reports/hatemail his way.
* [Alex Colavin](http://web.stanford.edu/~acolavin/)
* Maya Anjur-Dietrich
* Handuo Shi
