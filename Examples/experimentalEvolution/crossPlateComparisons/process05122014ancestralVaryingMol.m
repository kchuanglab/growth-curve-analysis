function pds = process05122014ancestralVaryingMol()

%% Define Sample generators
% Common properties of all Samples on this plate
strain = 2;
generation = 0;
testGlucose = 2500;
missingComponent = 'none';

[props, sample] = initializeSampleProps();

% Anonymous functions for brevity
prolprol = @(evolvedOsmolarity, testOsmolarity, wells)...
  sample(wells, props(Osmolyte.Proline, evolvedOsmolarity, Osmolyte.Proline, testOsmolarity, testGlucose, missingComponent, strain, generation));
sorbsorb = @(evolvedOsmolarity, testOsmolarity, wells)...
  sample(wells, props(Osmolyte.Sorbitol, evolvedOsmolarity, Osmolyte.Sorbitol, testOsmolarity, testGlucose, missingComponent, strain, generation));

%% Specify the plate layout
plate = Plate([...
  prolprol(0.0, 0.1, [Well('B2'), Well('C2'), Well('D2')])
  prolprol(0.0, 0.2, [Well('B3'), Well('C3'), Well('D3')])
  prolprol(0.0, 0.3, [Well('B4'), Well('C4'), Well('D4')])
  prolprol(0.0, 0.4, [Well('B5'), Well('C5'), Well('D5')])
  prolprol(0.0, 0.5, [Well('B6'), Well('C6'), Well('D6')])
  prolprol(0.0, 0.6, [Well('B7'), Well('C7'), Well('D7')])
  prolprol(0.0, 0.7, [Well('B8'), Well('C8'), Well('D8')])
  prolprol(0.0, 0.8, [Well('B9'), Well('C9'), Well('D9')])
  prolprol(0.0, 0.9, [Well('B10'), Well('C10'), Well('D10')])
  prolprol(0.0, 0.0, [Well('B11'), Well('C11'), Well('D11')])
  sorbsorb(0.0, 0.1, [Well('E2'), Well('F2'), Well('G2')])
  sorbsorb(0.0, 0.2, [Well('E3'), Well('F3'), Well('G3')])
  sorbsorb(0.0, 0.3, [Well('E4'), Well('F4'), Well('G4')])
  sorbsorb(0.0, 0.4, [Well('E5'), Well('F5'), Well('G5')])
  sorbsorb(0.0, 0.5, [Well('E6'), Well('F6'), Well('G6')])
  sorbsorb(0.0, 0.6, [Well('E7'), Well('F7'), Well('G7')])
  sorbsorb(0.0, 0.7, [Well('E8'), Well('F8'), Well('G8')])
  sorbsorb(0.0, 0.8, [Well('E9'), Well('F9'), Well('G9')])
  sorbsorb(0.0, 0.9, [Well('E10'), Well('F10'), Well('G10')])
  sorbsorb(0.0, 0.0, [Well('E11'), Well('F11'), Well('G11')])]);

%% Extract data
allTimeseries = importGrowthData('varyingMolarities05122014ANC.xlsx', 42, 86, 'BK');
pds = PlateDataSet(allTimeseries, plate, struct('general', struct('manualName', '05122014, ancestor, varyingMol')));

end