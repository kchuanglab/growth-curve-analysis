%% Load data into PlateDataSets
gen116_pds1 = process05292014evolvedGen116SwitchedOsm();
gen116_pds2 = process06252014evolvedGen116SwitchedOsm();

%% Aggregate the PlateDataSets for comparisons across evolution generations
% Parameters for same osmolyte comparisons
crossplate_pds = [gen116_pds1, gen116_pds2];
crossplate_props = struct('general', struct('manualName', 'Cross-plate replicates'));
% Generate the AggregatePlateDataSet
crossplate_apds = aggregatePlateDataSets(crossplate_pds, crossplate_props);

%% Generate plots
odTimeseriesProps = struct(...
  'Figure', struct('Position', [50 50 1600 900],...
                   'Title', struct('Size', 18),...
                   'Legend', struct('Size', 14),...
                   'Antialiasing', struct('Antialias', true,...
                                          'Supersampling', 2)),...
  'Axes', struct('Limits', struct('X', 'auto',...
                                  'Y', [0 2.5]),...
                 'Labels', struct('XSize', 16,...
                                  'YSize', 16),...
                 'Ticks', struct('LabelSize', 12)),...
  'Processing', struct('Smoothing', true),...
  'Lineseries', struct('Lines', struct('Style', '-',...
                                       'Width', 3),...
                       'Markers', struct('Marker', 'none',...
                                         'EdgeColor', 'none',...
                                         'FaceColor', 'none',...
                                         'Size', 4)),...
  'Timeseries', struct('Rescale', true,...
                       'Annotations', struct('MaxGrowthRate', struct('NumberOfMaxGrowthRates', 1))),...
  'Color', struct('Colormap', struct('Type', 'qual',...
                                     'Name', 'Set1'),...
                  'Background', 'white'));
maxGrowthRateProps = struct(...
  'Figure', struct('Position', [50 50 1600 900],...
                   'Title', struct('Size', 18),...
                   'Antialiasing', struct('Antialias', true,...
                                          'Supersampling', 2)),...
  'Axes', struct('Limits', struct('X', 'auto',...
                                  'Y', [0 1]),...
                 'Labels', struct('XSize', 16,...
                                  'YSize', 16),...
                 'Ticks', struct('LabelSize', 12,...
                                 'XLabelRotation', 30)),...
  'Processing', struct('Smoothing', true),...
  'Timeseries', struct('Averaging', true),...
  'Color', struct('Background', 'white'));
% Plot the two plates together
plotODTimeseries(crossplate_apds, odTimeseriesProps, 'DataSet');
% Plot each aggregated Sample separately
odTimeseriesProps.Timeseries.Averaging = struct('Lines', struct('Style', '--',...
                                                                'Width', 3));
crossplate_apds_samples = aggregateDataSets(crossplate_apds.dataSets, '');
plotODTimeseries(crossplate_apds_samples, odTimeseriesProps, 'Well');
plotMaxGrowthRates(crossplate_apds_samples, maxGrowthRateProps, 'Well');

%% Generate growth rate plots of individual aggregated Samples re-aggregated by testOsmolyte
crossplate_apds_sample_testOsmolyte = aggregateDataSets(crossplate_apds_samples, 'Measurement.Medium.Osmolyte.Type');
maxGrowthRateProps.Figure.Title.Color = 'white';
maxGrowthRateProps.Legend.Size = 14;
maxGrowthRateProps.Axes.Limits.X = [0 1];
maxGrowthRateProps.Axes.Labels.XColor = 'white';
maxGrowthRateProps.Axes.Labels.YColor = 'white';
maxGrowthRateProps.Axes.Ticks = rmfield(maxGrowthRateProps.Axes.Ticks, 'XLabelRotation');
maxGrowthRateProps.Processing.Averaging = false;
maxGrowthRateProps.Lineseries = struct('Lines', struct('Style', '-',...
                                                       'Width', 3),...
                                       'Markers', struct('Marker', 'o',...
                                                         'EdgeColor', 'black',...
                                                         'FaceColor', 'none',...
                                                         'Size', 6),...
                                       'Errorbars', true);
maxGrowthRateProps.Color = struct('Colormap', struct('Type', 'seq',...
                                                     'Name', 'PuBuGn'),...
                                  'Background', [0.5 0.5 0.5]);
plotMaxGrowthRatesByOsmolarity(crossplate_apds_sample_testOsmolyte, maxGrowthRateProps);
