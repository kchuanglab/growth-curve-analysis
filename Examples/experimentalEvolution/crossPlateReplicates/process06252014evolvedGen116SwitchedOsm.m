function pds = process06252014evolvedGen116SwitchedOsm()

%% Define Sample generators
% Common properties of all Samples on this plate
strain = 2;
generation = 116;
testGlucose = 2500;
missingComponent = 'none';

[props, sample] = initializeSampleProps();

% Anonymous functions for brevity
sorbprol = @(evolvedOsmolarity, testOsmolarity, wells)...
  sample(wells, props(Osmolyte.Sorbitol, evolvedOsmolarity, Osmolyte.Proline, testOsmolarity, testGlucose, missingComponent, strain, generation));
prolsorb = @(evolvedOsmolarity, testOsmolarity, wells)...
  sample(wells, props(Osmolyte.Proline, evolvedOsmolarity, Osmolyte.Sorbitol, testOsmolarity, testGlucose, missingComponent, strain, generation));

%% Specify the plate layout
plate = Plate([...
  prolsorb(0.5, 0.1, [Well('B2'), Well('C2'), Well('D2')])
  prolsorb(0.5, 0.2, [Well('B3'), Well('C3'), Well('D3')])
  prolsorb(0.5, 0.3, [Well('B4'), Well('C4'), Well('D4')])
  prolsorb(0.5, 0.4, [Well('B5'), Well('C5'), Well('D5')])
  prolsorb(0.5, 0.5, [Well('B6'), Well('C6'), Well('D6')])
  prolsorb(0.5, 0.6, [Well('B7'), Well('C7'), Well('D7')])
  prolsorb(0.5, 0.7, [Well('B8'), Well('C8'), Well('D8')])
  prolsorb(0.5, 0.8, [Well('B9'), Well('C9'), Well('D9')])
  prolsorb(0.5, 0.9, [Well('B10'), Well('C10'), Well('D10')])
  prolsorb(0.5, 0.0, [Well('B11'), Well('C11'), Well('D11')])
  sorbprol(0.5, 0.1, [Well('E2'), Well('F2'), Well('G2')])
  sorbprol(0.5, 0.2, [Well('E3'), Well('F3'), Well('G3')])
  sorbprol(0.5, 0.3, [Well('E4'), Well('F4'), Well('G4')])
  sorbprol(0.5, 0.4, [Well('E5'), Well('F5'), Well('G5')])
  sorbprol(0.5, 0.5, [Well('E6'), Well('F6'), Well('G6')])
  sorbprol(0.5, 0.6, [Well('E7'), Well('F7'), Well('G7')])
  sorbprol(0.5, 0.7, [Well('E8'), Well('F8'), Well('G8')])
  sorbprol(0.5, 0.8, [Well('E9'), Well('F9'), Well('G9')])
  sorbprol(0.5, 0.9, [Well('E10'), Well('F10'), Well('G10')])
  sorbprol(0.5, 0.0, [Well('E11'), Well('F11'), Well('G11')])]);

%% Extract data
allTimeseries = importGrowthData('switchedOsmolytes06252014gen116_run2.xlsx', 42, 86, 'BK');
pds = PlateDataSet(allTimeseries, plate, struct('general', struct('manualName', '06252014, gen116, switchedOsm')));

end