function pds = spreadsheetprocess140715_mlaA_mutt_growth()

%% Specify the plate layout
plate = generatePlateFromExcelSpreadsheet('plate layout.xlsx', 8, 12);

%% Extract data
allTimeseries = importGrowthData('140715_mlaA_mut_growth.xlsx', 43, 103, 'CU');
pds = PlateDataSet(allTimeseries, plate, struct('general', struct('manualName', '140715 mlaA mut growth')));

end