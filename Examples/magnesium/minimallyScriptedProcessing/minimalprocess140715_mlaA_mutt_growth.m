function pds = minimalprocess140715_mlaA_mutt_growth()

%% Define Sample generators
sample = @(strain, mg, wells) Sample(wells, generateProps(strain, mg), generateProps());

%% Specify the plate layout
plate = Plate([...
  sample('control', false, [Well('A1'), Well('A2'), Well('A3'), Well('A4'), Well('A5'), Well('A6'), Well('E1'), Well('E2'), Well('E3'), Well('E4'), Well('E5'), Well('E6')])
  sample('control', true, [Well('A7'), Well('A8'), Well('A9'), Well('A10'), Well('A11'), Well('A12'), Well('E7'), Well('E8'), Well('E9'), Well('E10'), Well('E11'), Well('E12')])
  sample('mlaAmut mg+', false, [Well('B1'), Well('B2'), Well('B3'), Well('B4'), Well('B5'), Well('B6'), Well('F1'), Well('F2'), Well('F3'), Well('F4'), Well('F5'), Well('F6')])
  sample('mlaAmut mg+', true, [Well('B7'), Well('B8'), Well('B9'), Well('B10'), Well('B11'), Well('B12'), Well('F7'), Well('F8'), Well('F9'), Well('F10'), Well('F11'), Well('F12')])
  sample('mlaAmut mg-', false, [Well('C1'), Well('C2'), Well('C3'), Well('C4'), Well('C5'), Well('C6'), Well('G1'), Well('G2'), Well('G3'), Well('G4'), Well('G5'), Well('G6')])
  sample('mlaAmut mg-', true, [Well('C7'), Well('C8'), Well('C9'), Well('C10'), Well('C11'), Well('C12'), Well('G7'), Well('G8'), Well('G9'), Well('G10'), Well('G11'), Well('G12')])
  sample('Delta mlaA', false, [Well('D1'), Well('D2'), Well('D3'), Well('D4'), Well('D5'), Well('D6'), Well('H1'), Well('H2'), Well('H3'), Well('H4'), Well('H5'), Well('H6')])
  sample('Delta mlaA', true, [Well('D7'), Well('D8'), Well('D9'), Well('D10'), Well('D11'), Well('D12'), Well('H7'), Well('H8'), Well('H9'), Well('H10'), Well('H11'), Well('H12')])]);

%% Extract data
allTimeseries = importGrowthData('140715_mlaA_mut_growth.xlsx', 43, 103, 'CU');
pds = PlateDataSet(allTimeseries, plate, struct('general', struct('manualName', '140715 mlaA mut growth')));

end

function props = generateProps(strain, mg)
if nargin == 0
  strain = 'uninitialized';
  mg = 'undefined';
end

props = struct('Strain', strain, 'Mg', mg);
end