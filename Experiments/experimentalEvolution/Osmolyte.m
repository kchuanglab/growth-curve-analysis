classdef Osmolyte
  %Osmolyte Enumeration to specify the osmolyte used in a given sample.
  %  Options: GlycineBetaine, Sorbitol, NaCl, Sucrose, Proline, None,
  %  Uninitialized
  
  enumeration
    GlycineBetaine, Sorbitol, NaCl, Sucrose, Proline, None, Uninitialized
  end
  
end
