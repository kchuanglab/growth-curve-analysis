function [props, sample] = initializeSampleProps()
%initializeSampleProps Generates the functions generation of Sample objects
%                      with custom properties.

props = @generateProps;
sample = @generateSample;

end

function sample = generateSample(wells, props)
sample = Sample(wells, props, generateProps(), @namingFunction, @initializationSensitiveNamingFunction);
end

function props = generateProps(evolvedOsmolyte, evolvedOsmolarity, testOsmolyte, testOsmolarity,...
  testGlucose, missingComponent, strain, generation)
%initializeProps Generate a properties struct for Sample
% When no arguments are given, returns a properties struct in which all
% values have their uninitialized value.
% evolvedOsmolyte should be a value from the Osmolyte enumeration
%                 corresponding to the osmolyte of the medium in which the
%                 strain was evolved.
% evolvedOsmolarity corresponds to the concentration of the osmolyte of the
%                   medium in which the strain was evolved. Units: M
% testOsmolyte should be a value from the Osmolyte enumeration
%              corresponding to the osmolyte of the medium in which the
%              strain was grown in the plate reader.
% testOsmolarity corresponds to the concentration of the osmolyte of the
%                medium in which the strain was grown in the plate reader.
%                Units: M
% testGlucose correponds to the concentration of glucose of the medium in
%             which the strain was grown in the plate reader. Units: mg/L
% missingComponent is the name of whatever component is missing from the DM
%                  medium in which the strain was grown in the plate
%                  reader. Use 'none' if there are no missing components.
% strain should be an identifier for the bacterial strain measured.
% generation should be the number of passages the strain has undergone in
%            the course of its evolution (0 for ancestral).

%% Pre-process arguments
if nargin == 0
  evolvedOsmolyte = Osmolyte.Uninitialized;
  evolvedOsmolarity = -1;
  testOsmolyte = Osmolyte.Uninitialized;
  testOsmolarity = -1;
  testGlucose = -1;
  missingComponent = 'uninitialized';
  strain = -1;
  generation = -1;
end

%% Initialize properties struct
props = struct('Strain', strain,...
  'Measurement', struct('Medium', struct('Osmolyte', struct('Type', testOsmolyte, 'Concentration', testOsmolarity), 'Glucose', testGlucose, 'MissingComponent', missingComponent)),...
  'Evolution', struct('Medium', struct('Osmolyte', struct('Type', evolvedOsmolyte, 'Concentration', evolvedOsmolarity)), 'Generation', generation));
end

function name = namingFunction(props)
% namingFunction Generates a name for a Sample based on the values of the
%                input properties struct
name = [...
  char(props.Measurement.Medium.Osmolyte.Type) ' ' num2str(props.Measurement.Medium.Osmolyte.Concentration) 'M'...
  ' in DM' num2str(props.Measurement.Medium.Glucose)];
if ~strcmp(props.Measurement.Medium.MissingComponent, 'none')
  name = [name '-' props.Measurement.Medium.MissingComponent];
end
name = [...
  name...
  ', strain ' int2str(props.Strain) ' from '...
  char(props.Evolution.Medium.Osmolyte.Type) ' ' num2str(props.Evolution.Medium.Osmolyte.Concentration) 'M'...
  ' gen ' num2str(props.Evolution.Generation)
  ];
end

function name = initializationSensitiveNamingFunction(props, uninitialized)
% initializationSensitiveNamingFunction Generates a name for a Sample based
%                                       on the initialized values of the
%                                       input properties struct
  name = '';
  if props.Measurement.Medium.Osmolyte.Type ~= uninitialized.Measurement.Medium.Osmolyte.Type
    name = [name char(props.Measurement.Medium.Osmolyte.Type)];
    if props.Measurement.Medium.Osmolyte.Concentration ~= uninitialized.Measurement.Medium.Osmolyte.Concentration && props.Measurement.Medium.Glucose ~= uninitialized.Measurement.Medium.Glucose
      name = [name ' '];
    else
      name = [name ', '];
    end
  end
  if props.Measurement.Medium.Osmolyte.Concentration ~= uninitialized.Measurement.Medium.Osmolyte.Concentration
    name = [name num2str(props.Measurement.Medium.Osmolyte.Concentration) 'M'];
    name = [name ' '];
  end
  if props.Measurement.Medium.Glucose ~= uninitialized.Measurement.Medium.Glucose
    name = [name 'in DM' num2str(props.Measurement.Medium.Glucose)];
  end
  if ~strcmp(props.Measurement.Medium.MissingComponent, uninitialized.Measurement.Medium.MissingComponent) &&...
      ~strcmp(props.Measurement.Medium.MissingComponent, 'none')
    name = [name '-' props.Measurement.Medium.MissingComponent];
  end
  name = [name ', '];
  if props.Strain ~= uninitialized.Strain
    name = [name 'strain ' int2str(props.Strain) ' '];
  end
  if isPartiallyInitialized(props.Evolution.Medium, uninitialized.Evolution.Medium)
    name = [name 'from '];
  end
  if props.Evolution.Medium.Osmolyte.Type ~= uninitialized.Evolution.Medium.Osmolyte.Type
    name = [name char(props.Evolution.Medium.Osmolyte.Type)];
    if props.Evolution.Medium.Osmolyte.Concentration ~= uninitialized.Evolution.Medium.Osmolyte.Concentration
      name = [name ' '];
    else
      name = [name ', '];
    end
  end
  if props.Evolution.Medium.Osmolyte.Concentration ~= uninitialized.Evolution.Medium.Osmolyte.Concentration
    name = [name num2str(props.Evolution.Medium.Osmolyte.Concentration) 'M '];
  end
  if props.Evolution.Generation ~= uninitialized.Evolution.Generation
    name = [name 'gen ' num2str(props.Evolution.Generation)];
  end
end
