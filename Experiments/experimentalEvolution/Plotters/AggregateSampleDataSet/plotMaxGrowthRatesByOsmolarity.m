function h = plotMaxGrowthRatesByOsmolarity(ds, props, baseline)
%plotMaxGrowthRatesByOsmolarity Plots the max growth rate data of the input
%                               DataSet as a scatterplot against the
%                               osmolarity of each DataSet
%  Arguments:
%    ds: DataSet to be plotted.
%    props: a properties struct specifying optional parameters for figure
%           generation:
%           Figure
%             Position
%             Title
%               Size
%               Color
%             Legend
%               Size
%             Antialiasing
%               Supersampling
%           Axes
%             Limits
%               X
%               Y
%             Labels
%               XSize
%               YSize
%               XColor
%               YColor
%             Ticks
%               LabelSize
%               XLabelRotation
%           Processing
%             Smoothing
%           Lineseries
%             Lines
%               Style
%               Width
%             Markers
%               Marker
%               EdgeColor
%               FaceColor
%               Size
%             Errorbars
%           Color
%             Colormap
%             Background
%           Saving
%             Prefix
%             Filename
%             Suffix
%             Extension
%             AlsoSaveAsFig
%    baseline: AggregateSampleDataSet whose
%              constituent DataSets constitute a max growth rate vs.
%              osmolarity curve that should be set as a baseline of 0. This
%              baseline curve will be subtracted from the curves for the
%              constituent DataSets in ds before those curves are plotted.
%              The baseline curve should have the same set of osmolarities
%              as the other curves.

if isa(ds, 'containers.Map')
  for actualDS = values(ds)
    if nargin < 3
      plotMaxGrowthRatesByOsmolarity(actualDS{:}, props);
    else
      plotMaxGrowthRatesByOsmolarity(actualDS{:}, props, baseline);
    end
  end
else
  %% Process special flags in props
  smoothing = isfield(props, 'Processing') && isfield(props.Processing, 'Smoothing') && props.Processing.Smoothing;
  plotErrorBars = isfield(props, 'Lineseries') && isfield(props.Lineseries, 'Errorbars') && (isstruct(props.Lineseries.Errorbars) || props.Lineseries.Errorbars);
  normalizeByBaseline = nargin > 2;
  
  %% Prepare data
  dataSets = ds.dataSets;
  dataSetsKeys = keys(dataSets);
  grouping = cell(numel(dataSetsKeys), 1);
  testOsmolarities = zeros(numel(dataSetsKeys), 1);
  maxGrowthRates = zeros(numel(dataSetsKeys), 1);
  if plotErrorBars
    errors = zeros(numel(dataSetsKeys), 1);
  end
  if normalizeByBaseline
    baselineDataSets = baseline.dataSets;
    baselineDataSetsKeys = keys(baselineDataSets);
    baselineTestOsmolarities = zeros(numel(baselineDataSetsKeys), 1);
    baselineMaxGrowthRates = zeros(numel(baselineDataSetsKeys), 1);
  end
  % Calculate testOsmolarity & maxGrowthRate (and, conditionally, error)
  % for each DataSet
  for i = 1:numel(dataSetsKeys)
    currentDataSet = dataSets(dataSetsKeys{i});
    currentRepresentativeSample = currentDataSet.representativeSample();
    testOsmolarities(i) = currentRepresentativeSample.props.Measurement.Medium.Osmolyte.Concentration;
    maxGrowthRates(i) = currentDataSet.averageMaxGrowthRate(smoothing);
    if plotErrorBars
      errors(i) = std(cell2mat(values(currentDataSet.wellMaxGrowthRates(smoothing))));
    end
    % Grouping: each group should have all properties the same except for
    % the osmolyte concentration in the medium under the plate reader; each
    % group gets its own max growth rate vs. osmolarity curve in the plot.
    currentRepresentativeSample.props.Measurement.Medium.Osmolyte.Concentration = currentRepresentativeSample.uninitialized.Measurement.Medium.Osmolyte.Concentration;
    grouping{i} = currentRepresentativeSample.initializedName;
  end
  if normalizeByBaseline
    for i = 1:numel(baselineDataSetsKeys)
      currentBaselineDataSet = baselineDataSets(baselineDataSetsKeys{i});
      currentBaselineRepresentativeSample = currentBaselineDataSet.representativeSample();
      baselineTestOsmolarities(i) = currentBaselineRepresentativeSample.props.Measurement.Medium.Osmolyte.Concentration;
      baselineMaxGrowthRates(i) = currentBaselineDataSet.averageMaxGrowthRate(smoothing);
      if plotErrorBars
        baselineErrors(i) = std(cell2mat(values(currentBaselineDataSet.wellMaxGrowthRates(smoothing))));
      end
    end
  end
  % Sort data points by their corresponding testOsmolarities
  [testOsmolaritiesSorted, sortedIndices] = sort(testOsmolarities);
  maxGrowthRatesSorted = maxGrowthRates(sortedIndices);
  if plotErrorBars
    errorsSorted = errors(sortedIndices);
  end
  if normalizeByBaseline
    [baselineTestOsmolaritiesSorted, baselineSortedIndices] = sort(baselineTestOsmolarities);
    baselineMaxGrowthRatesSorted = baselineMaxGrowthRates(baselineSortedIndices);
    if plotErrorBars
      baselineErrorsSorted = baselineErrors(baselineSortedIndices);
    end
  end
  groupingSorted = grouping(sortedIndices);
  % Define plotting groups
  groups = unique(groupingSorted);
  
  %% Set names for labels
  legendNames = groups;
  titleName = ['Max Growth Rates: ' ds.name];
  if normalizeByBaseline
    titleName = ['Normalized ' titleName];
  end
  if smoothing
    titleName = ['Smoothed ' titleName];
  end
  xUnits = 'M';
  yUnits = 'unitless per hour';
  yAxisName = strcat('Max Growth Rate (', yUnits, ')');
  if normalizeByBaseline
    yAxisName = ['Difference in ' yAxisName];
  end
  xAxisName = strcat('Test Medium Osmolarity (', xUnits, ')');
  
  %% Set up figure
  h = makeFigure(props);
  hold all;
  
  %% Plot data
  if ~isfield(props, 'Lineseries')
    props.Lineseries = struct();
  end
  plottingProperties = extractPlottingProperties(props.Lineseries);
  % Regroup data points by their grouping strings so that each group is a
  % column of a cell array
  for i = 1:numel(groups)
    currentDataPoints = strcmp(groupingSorted, groups{i});
    currentTestOsmolarities = testOsmolaritiesSorted(currentDataPoints);
    currentMaxGrowthRates = maxGrowthRatesSorted(currentDataPoints);
    if normalizeByBaseline
      currentMaxGrowthRates = currentMaxGrowthRates - baselineMaxGrowthRatesSorted;
    end
    if plotErrorBars
      currentErrors = errorsSorted(currentDataPoints);
      errorbarseries(i) = errorbar(currentTestOsmolarities, currentMaxGrowthRates, currentErrors);
      set(errorbarseries(i), plottingProperties{:});
    else
      plot(currentTestOsmolarities, currentMaxGrowthRates, plottingProperties{:});
    end
  end
  
  %% Finish plotting
  hold off;
  refineColors(h, gca, props, numel(legendNames));
  % Set labels
  setTitle(gca, props, titleName);
  refineAxes(gca, props, xAxisName, yAxisName);
  if plotErrorBars
    setLegend(gca, props, legendNames, errorbarseries);
  else
    setLegend(gca, props, legendNames);
  end
  renderAndSave(h, props);
end

end
