function h = plotLagTimesByOsmolarity(ds, props)
%plotLagTimesByOsmolarity Plots the lag time data of the input DataSet as a
%                         scatterplot against the osmolarity of each
%                         DataSet.
%  Arguments:
%    ds: DataSet to be plotted.
%    props: a properties struct specifying optional parameters for figure
%           generation:
%           Figure
%             Position
%             Title
%               Size
%               Color
%             Legend
%               Size
%             Antialiasing
%               Supersampling
%           Axes
%             Limits
%               X
%               Y
%             Labels
%               XSize
%               YSize
%               XColor
%               YColor
%             Ticks
%               LabelSize
%               XLabelRotation
%           Processing
%             Smoothing
%           Lineseries
%             Lines
%               Style
%               Width
%             Markers
%               Marker
%               EdgeColor
%               FaceColor
%               Size
%             Errorbars
%           Color
%             Colormap
%             Background
%           Saving
%             Prefix
%             Filename
%             Suffix
%             Extension
%             AlsoSaveAsFig

if isa(ds, 'containers.Map')
  for actualDS = values(ds)
    plotLagTimesByOsmolarity(actualDS{:}, props);
  end
else
  %% Process special flags in props
  smoothing = isfield(props, 'Processing') && isfield(props.Processing, 'Smoothing') && props.Processing.Smoothing;
  plotErrorBars = isfield(props, 'Lineseries') && isfield(props.Lineseries, 'Errorbars') && (isstruct(props.Lineseries.Errorbars) || props.Lineseries.Errorbars);
  
  %% Prepare data
  dataSets = ds.dataSets;
  dataSetsKeys = keys(dataSets);
  grouping = cell(numel(dataSetsKeys), 1);
  testOsmolarities = zeros(numel(dataSetsKeys), 1);
  lagTimes = zeros(numel(dataSetsKeys), 1);
  if plotErrorBars
    errors = zeros(numel(dataSetsKeys), 1);
  end
  % Calculate testOsmolarity & lagTime (and, conditionally, error) for each
  % DataSet
  for i = 1:numel(dataSetsKeys)
    currentDataSet = dataSets(dataSetsKeys{i});
    currentRepresentativeSample = currentDataSet.representativeSample();
    % testOsmolarity
    testOsmolarities(i) = currentRepresentativeSample.testMedium.osmolyteConc;
    % Lag Time
    lagTimes(i) = currentDataSet.lagTime(smoothing);
    % Error
    if plotErrorBars
      errors(i) = std(cell2mat(values(currentDataSet.wellLagTimes(smoothing))));
    end
    % Grouping: each group should have all properties the same except for
    % the osmolyte concentration in the medium under the plate reader
    currentRepresentativeSample.props.Measurement.Medium.Osmolyte.Concentration = currentRepresentativeSample.uninitialized.Measurement.Medium.Osmolyte.Concentration;
    grouping{i} = currentRepresentativeSample.initializedName;
    if isprop(currentDataSet, 'plateName')
      grouping{i} = [currentDataSet.plateName ': ' grouping{i}];
    end
  end
  % Sort data points by their corresponding testOsmolarities
  [testOsmolaritiesSorted, sortedIndices] = sort(testOsmolarities);
  lagTimesSorted = lagTimes(sortedIndices);
  if plotErrorBars
    errorsSorted = errors(sortedIndices);
  end
  groupingSorted = grouping(sortedIndices);
  % Define plotting groups
  groups = unique(groupingSorted);
  
  %% Set names for labels
  legendNames = groups;
  titleName = ['Lag Times: ' ds.name];
  if smoothing
    titleName = ['Smoothed ' titleName];
  end
  xUnits = 'M';
  yUnits = 'h';
  yAxisName = strcat('Lag Time (', yUnits, ')');
  xAxisName = strcat('Test Medium Osmolarity (', xUnits, ')');
  
  %% Set up figure
  h = makeFigure(props);
  hold all;
  
  %% Plot data
  if ~isfield(props, 'Lineseries')
    props.Lineseries = struct();
  end
  plottingProperties = extractPlottingProperties(props.Lineseries);
  % Regroup data points by their grouping strings so that each group is a
  % column of a cell array
  for i = 1:numel(groups)
    currentDataPoints = strcmp(groupingSorted, groups{i});
    currentTestOsmolarities = testOsmolaritiesSorted(currentDataPoints);
    currentLagTimes = lagTimesSorted(currentDataPoints);
    if plotErrorBars
      currentErrors = errorsSorted(currentDataPoints);
      errorbarseries(i) = errorbar(currentTestOsmolarities, currentLagTimes, currentErrors);
      set(errorbarseries(i), plottingProperties{:});
    else
      plot(currentTestOsmolarities, currentLagTimes, plottingProperties{:});
    end
  end
  
  %% Finish plotting
  hold off;
  refineColors(h, gca, props, numel(legendNames));
  % Set labels
  setTitle(gca, props, titleName);
  refineAxes(gca, props, xAxisName, yAxisName);
  if plotErrorBars
    setLegend(gca, props, legendNames, errorbarseries);
  else
    setLegend(gca, props, legendNames);
  end
  renderAndSave(h, props);
end

end
