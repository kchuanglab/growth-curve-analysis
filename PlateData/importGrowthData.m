function wellTimeseries = importGrowthData(spreadsheetPath, header, cycles, lastColumn)
%importGrowthData Generates a string->timeseries map of well timeseries
%                 from a spreadsheet; keys should be the names of Wells
%   Arguments:
%     spreadsheetPath: path to the spreadsheet to import. Raw data from the
%                      plate reader must be in the first worksheet.
%     header: the row that contains the header, which looks like 'Cycle
%     Nr.'|'Time [s]'|'Temp. [�C]'|...
%     cycles: the number of cycles that were run.
%     lastColumn: the last column of the table, e.g. 'CM'
%   Best used in combination, e.g. PlateDataSet(importGrowthData('foo', 5, 'BA'), plateLayout)

% Extract metadata
%[~, startTime] = xlsread(spreadsheetPath, 1, 'B39');
%startTime = datevec(startTime, 'mm/dd/yyyy HH:MM:SS PM');
%endTimeRange = strcat('B', int2str(42 + cycles + 4));
%[~, endTime] = xlsread(spreadsheetPath, 1, endTimeRange);
%endTime = datevec(endTime, 'mm/dd/yyyy HH:MM:SS PM');
%duration = etime(endTime, startTime);
%wavelength = xlsread(spreadsheetPath, 1, 'E34');
%bandwidth = xlsread(spreadsheetPath, 1, 'E35');

% Extract data
cycleNumbersRange = strcat('A', int2str(header + 1), ':A', int2str(header + cycles));
cycleNumbers = xlsread(spreadsheetPath, 1, cycleNumbersRange);
timesRange = strcat('B', int2str(header + 1), ':B', int2str(header + cycles));
times = xlsread(spreadsheetPath, 1, timesRange);
%temperaturesRange = strcat('C43:C', int2str(42 + cycles));
%temperatures = xlsread(spreadsheetPath, 1, temperaturesRange);
wellsRange = strcat('D', int2str(header), ':', lastColumn, int2str(header));
[~, wells] = xlsread(spreadsheetPath, 1, wellsRange);
absorbancesRange = strcat('D', int2str(header + 1), ':', lastColumn, int2str(header + cycles));
absorbances = xlsread(spreadsheetPath, 1, absorbancesRange);

% Split data into timeseries
numWells = size(wells, 2);
wellTimeseries = containers.Map();
for i = 1:numWells
  well = Well(wells{i});
  currWellTimeseries = timeseries(absorbances(:,i), times, 'Name', well.name);
  currWellTimeseries.TimeInfo.Units = 'seconds';
  %currWellTimeseries.TimeInfo.StartDate = startTime;
  currWellTimeseries.DataInfo.Units = 'unitless';
  currWellTimeseries.UserData = struct('well', well);
  % TODO: specify interpolation function here, too
  
  wellTimeseries(well.name) = currWellTimeseries;
end

end
