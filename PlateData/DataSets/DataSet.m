classdef (Abstract) DataSet
%DataSet Stores OD vs. time data.
%   Abbreviation: DS
%   Constructor arguments:
%     props: a structure

properties
  %% General properties
  manualName = ''
  %% Growth rate calculation parameters
  growthRateUpperCeiling = 1.5
  %% Data smoothing parameters
  smoothingPasses = 2
  smoothingSpan = 5
end

methods
  % CONSTRUCTOR
  function ds = DataSet(props)
    %% Pre-process arguments
    if nargin == 0
      props = struct();
    end
    
    %% Initialize local properties
    % General properties
    if isfield(props, 'general') && isfield(props.general, 'manualName')
      ds.manualName = props.general.manualName;
    end
    % Data smoothing parameters
    if isfield(props, 'smoothing') && isfield(props.smoothing, 'passes')
      ds.smoothingPasses = props.smoothing.passes;
    end
    if isfield(props, 'smoothing') && isfield(props.smoothing, 'span')
      ds.smoothingSpan = props.smoothing.span;
    end
  end
  
  function name = name(ds)
    if isempty(ds.manualName)
      name = ds.autoName;
    else
      name = ds.manualName;
    end
  end
end

methods (Abstract)
  % Generates an automatic name for the DataSet.
  autoName(ds)
  % Generates a fully-qualified name for the DataSet
  fullName(ds)
  % Generates a Sample, where each property's value is either the default
  % (uninitialized) value, or the value shared by the samples of all
  % constituent DataSets.
  representativeSample(ds)
  % Generates a string->timeseries Map of the scaled OD timeseries for all
  % wells associated with the DataSet; each key is the name of a Well,
  % prefixed by the name of the Plate to which it belongs.
  wellODTimeseries(ds, smoothing)
end

methods
  %% Individual well data
  % wellLagTimes
  %   Return: A string->number Map of the lag times for all wells; each
  %           key is the name of a Well, prefixed by the name of the
  %           Plate to which it belongs.
  %   Arguments:
  %     smoothing: a boolean. If true, the smoothed forms of constituent
  %                OD timeseries are used for calculation.
  function wellLagTimes = wellLagTimes(ds, smoothing)
    wellLagTimes = containers.Map();
    wellODTimeseries = ds.wellODTimeseries(smoothing);
    for currentKey = keys(wellODTimeseries)
      wellLagTimes(currentKey{:}) = calculateLagTime(wellODTimeseries(currentKey{:}));
    end
  end
  % wellGrowthRateTimeseries
  %   Return: A string->number Map of the growth rate timeseries for all
  %           wells; each key is the name of a Well, prefixed by the name
  %           of the Plate to which it belongs.
  %   Arguments:
  %     smoothing: a boolean. If true, the smoothed forms of constituent
  %                OD timeseries are used for calculation.
  function wellGrowthRateTimeseries = wellGrowthRateTimeseries(ds, smoothing)
    wellGrowthRateTimeseries = containers.Map();
    wellODTimeseries = ds.wellODTimeseries(smoothing);
    for currentKey = keys(wellODTimeseries)
      wellGrowthRateTimeseries(currentKey{:}) = scaledODsToGrowthRates(wellODTimeseries(currentKey{:}));
    end
  end
  % wellMaxGrowthRates
  %   Return: A string->number Map of the max growth rates for all wells;
  %           each key is the name of a Well, prefixed by the name of the
  %           Plate to which it belongs.
  %   Arguments:
  %     smoothing: a boolean. If true, the smoothed forms of constituent
  %                OD timeseries are used for calculation.
  function wellMaxGrowthRates = wellMaxGrowthRates(ds, smoothing)
    wellMaxGrowthRates = containers.Map();
    wellGrowthRateTimeseries = ds.wellGrowthRateTimeseries(smoothing);
    for currentKey = keys(wellGrowthRateTimeseries)
      [~, wellMaxGrowthRates(currentKey{:})] = findMaxGrowthRate(wellGrowthRateTimeseries(currentKey{:}), ds.growthRateUpperCeiling);
    end
  end
  %% DataSet representative data
  % averageODTimeseries
  %   Return: The average OD timeseries of the OD timeseries of each Well
  %           associated with the DataSet.
  %   Arguments:
  %     smoothing: a boolean. If true, the smoothed form of each well's OD
  %                timeseries is used for calculation.
  function averageODTimeseries = averageODTimeseries(ds, smoothing)
    averageODTimeseries = averageTimeseries(ds.wellODTimeseries(smoothing));
    averageODTimeseries.Name = [ds.name ', Average ODs'];
  end
  % averageLagTime
  %   Return: The average lag time of the lag times of the OD timeseries
  %           of each Well associated with the DataSet.
  %   Arguments:
  %     smoothing: a boolean. If true, the smoothed form of each well's OD
  %                timeseries is used for calculation.
  function averageLagTime = averageLagTime(ds, smoothing)
    averageLagTime = mean(cell2mat(values(ds.wellLagTimes(smoothing))));
  end
  % averageGrowthRateTimeseries
  %   Return: The average growth rate timeseries of the growth rate
  %           timeseries of each Well associated with the DataSet.
  %   Arguments:
  %     smoothing: a boolean. If true, the smoothed form of the average
  %                OD timeseries is used for calculation.
  function averageGrowthRateTimeseries = averageGrowthRateTimeseries(ds, smoothing)
    averageGrowthRateTimeseries = averageTimeseries(ds.wellGrowthRateTimeseries(smoothing));
    averageGrowthRateTimeseries.Name = [ds.name, ', Average of Growth Rates'];
  end
  % averageMaxGrowthRate
  %   Return:
  %     averageMaxGrowthRateTime: The average time of the times of the OD
  %                               timeseries of each Well associated with
  %                               the DataSet when the max growth rate was
  %                               reached.
  %     averageMaxGrowthRate: The average max growth rate of the max growth
  %                           rates of the OD timeseries of each Well
  %                           associated with the DataSet.
  %   Arguments:
  %     smoothing: a boolean. If true, the smoothed form of each well's OD
  %                timeseries is used for calculation.
  function averageMaxGrowthRate = averageMaxGrowthRate(ds, smoothing)
    averageMaxGrowthRate = mean(cell2mat(values(ds.wellMaxGrowthRates(smoothing))));
  end
end

end
