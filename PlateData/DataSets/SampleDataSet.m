classdef SampleDataSet < DataSet
  %SampleDataSet Stores timeseries data of a given sample.
  %   Abbreviation: SDS
  %   Constructor arguments:
  %     sample: A Sample.
  %     odTimeseries: A string->timeseries Map of OD timeseries for wells;
  %                   keys should be the names of Wells.
  %     props: Optional. A struct of optional properties to override class
  %            defaults.
  
  properties
    %% General properties
    sample
    plateName = ''
    %% Constituent data
    odTimeseries = containers.Map()
  end
  
  methods
    % CONSTRUCTOR
    function sds = SampleDataSet(sample, odTimeseries, props)
      %% Pre-process arguments
      if nargin < 3
        props = struct();
      end
      
      % Annotate each odTimeseries timeseries with Sample metadata
      for key = keys(odTimeseries)
        currentSeries = odTimeseries(key{:});
        currentSeries.UserData.Sample = sample;
        odTimeseries(key{:}) = currentSeries;
      end
      
      %% Initialize superclass
      sds = sds@DataSet(props);
      
      %% Initialize local properties
      % General properties
      sds.sample = sample;
      if isfield(props, 'general') && isfield(props.general, 'plateName')
        sds.plateName = props.general.plateName;
      end
      % Constituent data
      sds.odTimeseries = odTimeseries;
    end
    
    function name = fullName(sds)
      name = sds.name;
      if ~isempty(sds.plateName)
        name = [sds.plateName ': ' name];
      end
    end
    
    % IMPLEMENT DATASET
    function name = autoName(sds)
      name = sds.sample.name;
    end
    function sample = representativeSample(sds)
      sample = sds.sample;
    end
    % scaledTimeseries
    %   Return: A string->timeseries Map of the scaled OD timeseries for
    %           all wells; each key is the name of a Well, prefixed by the
    %           name of the Plate to which it belongs.
    %   Arguments:
    %     smoothing: a boolean. If true, the smoothed form of each scaled
    %                 OD timeseries is returned.
    function wellODTimeseries = wellODTimeseries(sds, smoothing)
      wellODTimeseries = containers.Map();
      for current = values(sds.odTimeseries)
        currentScaled = scaleODs(current{:});
        currentScaled.Name = [currentScaled.Name ', Scaled'];
        if smoothing
          currentScaled = smoothTimeseries(currentScaled, sds.smoothingPasses, sds.smoothingSpan);
          currentScaled.Name = [currentScaled.Name ', Smoothed'];
        end
        wellODTimeseries([sds.plateName ': ' currentScaled.Name]) = currentScaled;
      end
    end
  end
  
end
