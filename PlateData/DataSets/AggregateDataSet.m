classdef (Abstract) AggregateDataSet < DataSet
  %AggregateDataSet Aggregates multiple PlateDataSets. PlateDataSets
  %                      probably won't have the same time points.
  %   Abbreviation: ADS
  %   Constructor arguments:
  %     ds: A string->DataSet map. Keys should be the names of DataSets.
  %          PlateDataSets.
  %     props: Optional. A struct of optional properties to override class
  %            defaults.
  
  methods
    % CONSTRUCTOR
    function ads = AggregateDataSet(props)
      %% Pre-process arguments
      if nargin < 1
        props = struct();
      end
      
      %% Initialize superclass
      ads = ads@DataSet(props);
    end
    
    function sample = representativeSample(ads)
      dataSets = values(ads.dataSets);
      sample = dataSets{1}.representativeSample();
      for dataSet = dataSets
        dataSetSample = dataSet{:}.representativeSample();
        sample.props =...
          mergeProperties(sample.props, dataSetSample.props, dataSetSample.uninitialized);
      end
    end
    
    % IMPLEMENT DATASET
    function name = autoName(ads)
      name = ads.representativeSample().initializedName;
      name = ['Aggregate ' name];
    end
    function fullName = fullName(ads)
      fullName = ads.name();
    end
    
  end
  
end
