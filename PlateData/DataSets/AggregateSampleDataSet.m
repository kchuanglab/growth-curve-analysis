classdef AggregateSampleDataSet < AggregateDataSet
  %AggregateSampleDataSet Aggregates multiple DataSets (which should
  %                       probably be either AggregateSampleDataSets or
  %                       SampleDataSets).
  %   Abbreviation: ASDS
  %   Constructor arguments:
  %     ds: A string->DataSet map. Keys should be the names of DataSets.
  %     props: Optional. A struct of optional properties to override class
  %            defaults.
  
  properties
    %% Constituent data
    dataSets = containers.Map()
  end
  
  methods
    % CONSTRUCTOR
    function asds = AggregateSampleDataSet(ds, props)
      %% Pre-process arguments
      if nargin < 2
        props = struct();
      end
      
      %% Initialize superclass
      asds = asds@AggregateDataSet(props);
      
      %% Initialize local properties
      % Constituent data
      asds.dataSets = ds;
    end
    
    % IMPLEMENT DATASET
    % wellODTimeseries
    %   Return: A string->timeseries Map of the scaled OD timeseries for
    %           all wells of all constituent DataSets; each key is the name
    %           of a Well, prefixed by the name of the Plate to which it
    %           belongs.
    %   Arguments:
    %     smoothing: a boolean. If true, the smoothed forms of each 
    %                constituent DataSet's wells' OD timeseries are
    %                returned.
    function wellODTimeseries = wellODTimeseries(asds, smoothing)
      wellODTimeseries = containers.Map();
      for currentDataSet = values(asds.dataSets)
        currentWellODTimeseries = currentDataSet{:}.wellODTimeseries(smoothing);
        wellODTimeseries = [wellODTimeseries; currentWellODTimeseries];
      end
    end
    
    % dataSetODTimeseries
    %   Return: A string->timeseries Map of the average scaled OD
    %           timeseries for each constituent DataSet; each key is the
    %           name of a constituent DataSet.
    %   Arguments:
    %     smoothing: a boolean. If true, the smoothed forms of each
    %                constituent DataSet's wells' OD timeseries are used
    %                for calculation.
    function dataSetODTimeseries = dataSetODTimeseries(asds, smoothing)
      dataSetODTimeseries = containers.Map();
      for key = keys(asds.dataSets)
        dataSetODTimeseries(key{:}) = asds.dataSets(key{:}).averageODTimeseries(smoothing);
      end
    end
    % dataSetLagTimes
    %   Return: A string->number Map of the lag times for all wells of all
    %           constituent DataSets; keys are the names of constituent
    %           DataSets.
    %   Arguments:
    %     smoothing: a boolean. If true, the smoothed form of constituent
    %                OD series is used for calculation.
    function dataSetLagTimes = dataSetLagTimes(asds, smoothing)
      dataSetLagTimes = containers.Map();
      for key = keys(asds.dataSets)
        dataSetLagTimes(key{:}) = asds.dataSets(key{:}).averageLagTime(smoothing);
      end
    end
    % dataSetGrowthRateTimeseries
    %   Return: A string->timeseries Map of growth rate timeseries for all
    %           wells of all constituent DataSets; keys are the names of constituent DataSets.
    %   Arguments:
    %     smoothing: a boolean. If true, the smoothed form of constituent
    %                OD series is used for calculation.
    function dataSetGrowthRateTimeseries = dataSetGrowthRateTimeseries(asds, smoothing)
      dataSetGrowthRateTimeseries = containers.Map();
      for key = keys(asds.dataSets)
        dataSetGrowthRateTimeseries(key{:}) = asds.dataSets(key{:}).averageGrowthRateTimeseries(smoothing);
      end
    end
    % dataSetMaxGrowthRates
    %   Return: A string->number Map of maximum growth rates for wells;
    %           keys are the names of constituent DataSets.
    %   Arguments:
    %     smoothing: a boolean. If true, the smoothed form of constituent
    %                OD series is used for calculation.
    function dataSetMaxGrowthRates = dataSetMaxGrowthRates(asds, smoothing)
      dataSetMaxGrowthRates = containers.Map();
      for key = keys(asds.dataSets)
        dataSetMaxGrowthRates(key{:}) = asds.dataSets(key{:}).averageMaxGrowthRate(smoothing);
      end
    end
  end
  
end
