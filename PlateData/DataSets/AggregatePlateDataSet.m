classdef AggregatePlateDataSet < AggregateSampleDataSet
  %AggregatePlateDataSet Aggregates the SampleDataSets of multiple
  %                      PlateDataSets.
  %   Abbreviation: APDS
  %   Constructor arguments:
  %     pds: A string->PlateDataSet map. Keys should be the names of
  %          PlateDataSets.
  %     props: Optional. A struct of optional properties to override class
  %            defaults.
  
  properties
    %% Constituent data
    plateDataSets = containers.Map()
  end
  
  methods
    % CONSTRUCTOR
    function apds = AggregatePlateDataSet(pds, props)
      %% Pre-process arguments
      if nargin < 2
        props = struct();
      end
      
      %% Initialize superclass
      sampleDataSets = containers.Map();
      for currentPlateDataSet = values(pds)
        currentSampleDataSets = currentPlateDataSet{:}.dataSets;
        for currentSampleDataSet = values(currentSampleDataSets)
          sampleDataSets(currentSampleDataSet{:}.fullName) = currentSampleDataSet{:};
        end
      end
      apds = apds@AggregateSampleDataSet(sampleDataSets, props);
      
      %% Initialize local properties
      % Constituent data
      apds.plateDataSets = pds;
    end
  end
  
end
