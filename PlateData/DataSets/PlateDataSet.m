classdef PlateDataSet < AggregateSampleDataSet
  %PlateDataSet Stores timeseries data based on sample
  %   Abbreviation: PDS
  %   Constructor arguments:
  %     wellTimeseries: a string->timeseries Map of timeseries for wells;
  %                     keys should be the names of Wells
  %     plate: a Plate
  %     props: a properties struct for the PlateDataSet
  
  properties
    excludedWells = []
    plate = Plate()
  end
  
  methods
    % Constructor
    function pds = PlateDataSet(wellTimeseries, plate, props)
      %% Pre-process arguments
      if nargin < 3
        props = struct();
      end
      
      %% Initialize superclass
      pds = pds@AggregateSampleDataSet(containers.Map(), props);
      
      %% Initialize local properties
      if isfield(props, 'excludedWells')
        pds.excludedWells = props.excludedWells;
      end
      pds.plate = plate;
      % Initialize the SampleDataSets
      for sample = plate.samples
        currentODSeries = extractTimeseriesByWell(wellTimeseries, sample.wells, pds.excludedWells);
        % Check if the SampleDataSet will have at least one currentODSeries
        if ~isempty(keys(currentODSeries))
          sampleProps = props;
          sampleProps.general.plateName = pds.name();
          sampleProps.general = rmfield(sampleProps.general, 'manualName');
          pds.dataSets(sample.name) = SampleDataSet(sample, currentODSeries, sampleProps);
        else
          disp(['Skipped sample ' sample.name])
        end
      end
    end
  end

end
