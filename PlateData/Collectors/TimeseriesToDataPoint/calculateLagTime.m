function [lagTime, f, p] = calculateLagTime(scaledODs)
%calculateLagTime Calculates the lag time (i.e. the end boundary of lag
%                 phase) of a growth curve
%  Arguments:
%    scaledODs: scaled OD series

% Fit a Gompertz function to the data (ported from a script written by
% Carolina Tropini
f = @(p,time) p(4) + p(3) * exp(-exp(1 + p(2) * exp(1) .* (p(1) - time) / p(3)));
options = statset('nlinfit');
options.MaxIter = 1000;
options.RobustWgtFun = 'fair';
p = nlinfit(scaledODs.Time, scaledODs.Data, f, [0.1, 1 / max(scaledODs.Time), max(scaledODs.Data), min(scaledODs.Data)], options);

lagTime = p(1);
% rate = p(2);
% maxOD = p(3);
% initialOD = p(4);

end
