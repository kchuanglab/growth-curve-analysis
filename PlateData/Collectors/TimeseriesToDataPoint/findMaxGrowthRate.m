function [time, maxGrowthRate] = findMaxGrowthRate(growthRates, upperCeiling, n)
%findMaxGrowthRate Finds the maximum growth rate (as represented by the k-constant)
%                  of a timeseries.
%  Arguments:
%    growthRates: a timeseries of growth rates.
%    upperCeiling: the threshold above which growth rates are considered
%                  unrealistic.
%    n: an ordinal. optional. If specified, finds the nth max growth rate.
inputData = growthRates.Data;

[initialPeaks, initialIndices] = findpeaks(inputData, 'MINPEAKHEIGHT', 0);
% Discard peaks (and their corresponding indices) which are infinite or
% above upperCeiling
filter = (initialPeaks ~= Inf) & (initialPeaks < upperCeiling);
filteredPeaks = initialPeaks(filter);
filteredIndices = initialIndices(filter);

% Default to only finding the largest growth rate (as opposed to the
% second-largest, third-largest, etc., growth rate, if n is unspecified
if nargin < 3
  n = 1;
end

for i = 1:n
  % Find the highest peak and its corresponding time
  [maxGrowthRate, filteredIndex] = max(filteredPeaks);
  time = growthRates.Time(filteredIndices(filteredIndex));
  % Discard the peak and its corresponding index
  filteredIndices(filteredIndex) = [];
  filteredPeaks(filteredIndex) = [];
end
end
