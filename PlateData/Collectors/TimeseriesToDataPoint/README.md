TimeseriesToDataPoint
=========

Functions in this directory:

  - Take as their first argument a timeseries.
  - Output as their first return a time.
  - Optionally, output as their second return a value associated with that time.