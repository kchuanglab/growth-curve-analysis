PlateDataSetVectorToAggregatePlateDataSet
=========

Functions in this directory:

  - Take as their first argument a PlateDataSet vector.
  - Output as their first return an AggregatePlateDataSet.