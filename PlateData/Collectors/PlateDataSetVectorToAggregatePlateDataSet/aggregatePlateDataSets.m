function apds = aggregatePlateDataSets(plateDataSets, props)
%aggregatePlateDataSets Aggregates the input plateDataSets into an AggregatePlateDataSet
%  Arguments:
%    plateDataSets: a vector of the PlateDataSets to be aggregated
%    props: a structure

pdsMap = containers.Map();
for pds = plateDataSets
  pdsMap(pds.name()) = pds;
end
apds = AggregatePlateDataSet(pdsMap, props);

end