AggregateSampleDataSetVectorToAggregateSampleDataSet
=========

Functions in this directory:

  - Take as their first argument an AggregateSampleDataSet vector.
  - Output as their first return an AggregateSampleDataSet.