function asds = aggregateAggregateSampleDataSets(aggregateSampleDataSets, props)
%aggregateAggregateSampleDataSets Aggregates the input aggregateSampleDataSets into an AggregateSampleDataSet
%  Arguments:
%    aggregateSampleDataSets: a vector of the AggregateSampleDataSets to be aggregated
%    props: a structure

asdsMap = containers.Map();
for asds = aggregateSampleDataSets
  asdsMap(asds.name()) = asds;
end
asds = AggregateSampleDataSet(asdsMap, props);

end