function average = averageTimeseries(timeseries)
%averageTimeseries Averages the input.
%  Arguments:
%    timeseries: the string->timeseries Map of all timeseries to be
%                averaged. Any timeseries with different time vectors will
%                be resampled to the time vector of the first timeseries in
%                the Map.

timeseriesKeys = keys(timeseries);
n = size(timeseriesKeys, 2);
sum = timeseries(timeseriesKeys{1});
for i = 2:n
  currTimeseries = timeseries(timeseriesKeys{i});
  if ~isequal(sum.Time, currTimeseries.Time)
    currTimeseries = resample(currTimeseries, sum.Time);
  end
  sum = sum + currTimeseries;
end
average = sum ./ n;

end