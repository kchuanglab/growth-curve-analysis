TimeseriesMapToTimeseries
=========

Functions in this directory:

  - Take as their first argument a string->timeseries Map. Keys should be the names of the timeseries, which should be well names.
  - Output as their first return a timeseries.
  - Do not modify inputs (this is important because Map is a handle class).