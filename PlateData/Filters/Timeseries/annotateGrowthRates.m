function annotated = annotateGrowthRates(growthRates, props, dataSet)
%annotateGrowthRates Annotates the input growth rate series with events,
%                    according to the specified properties struct
%  Arguments:
%    growthRates: growth rate timeseries to be annotated.
%    props: a properties struct specifying optional parameters for
%           adding annotations:
%           Timeseries
%             Annotations
%               LagTime
%               MaxGrowthRate
%                 NumberOfMaxGrowthRates
%    dataSet: a DataSet
annotated = growthRates;

annotateMaxGrowthRate = isfield(props, 'Timeseries') && isfield(props.Timeseries, 'Annotations') && isfield(props.Timeseries.Annotations, 'MaxGrowthRate') && (isstruct(props.Timeseries.Annotations.MaxGrowthRate) || props.Timeseries.Annotations.MaxGrowthRate);

if annotateMaxGrowthRate
  if isfield(props.Timeseries.Annotations.MaxGrowthRate, 'NumberOfMaxGrowthRates')
    annotated = annotateGrowthRatesWithMaxGrowthRate(annotated, dataSet.growthRateUpperCeiling, props.Timeseries.Annotations.MaxGrowthRate.NumberOfMaxGrowthRates);
  else
    annotated = annotateGrowthRatesWithMaxGrowthRate(annotated, dataSet.growthRateUpperCeiling);
  end
end

end
