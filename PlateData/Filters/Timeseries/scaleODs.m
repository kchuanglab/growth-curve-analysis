function scaled = scaleODs(raw)
%scaleODs Scales the input OD series.
%  Arguments:
%    raw: OD series to be scaled.
scaled = raw;
scaled.Data = log(raw.Data) - log(min(raw));
scaled.Name = [raw.Name ' Scaled'];

end
