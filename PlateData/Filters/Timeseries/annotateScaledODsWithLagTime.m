function [annotated, f, p] = annotateScaledODsWithLagTime(scaledODs)
%annotateScaledODsWithLagTime Annotates the input scaled OD series with events.
%  Arguments:
%    scaledODs: scaled OD series to be annotated.
annotated = scaledODs;

%% Lag time metrics
[lagTime, f, p] = calculateLagTime(scaledODs);
annotated = addevent(annotated, 'Lag Phase Boundary', lagTime);

end
