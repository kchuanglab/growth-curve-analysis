function rescaled = rescaleTime(prescaled, newUnits)
%rescaleTime Rescales the input timeseries to a different unit of time
%  Arguments:
%    prescaled: timeseries to be scaled.
%    newUnits: time units to which the timeseries should be scaled. Should
%              be 'hours', 'minutes', or 'seconds'.
rescaled = prescaled;
rescaled.TimeInfo.Units = newUnits;
oldUnits = prescaled.TimeInfo.Units;

% TODO: this implementation is terrible and lazy, replace it
if strcmp(oldUnits, 'seconds')
  if strcmp(newUnits, 'hours')
    rescaled.Time = rescaled.Time / 3600;
  elseif strcmp(newUnits, 'minutes')
    rescaled.Time = rescaled.Time / 60;
  end
elseif strcmp(oldUnits, 'minutes')
  if strcmp(newUnits, 'hours')
    rescaled.Time = rescaled.Time / 60;
  elseif strcmp(newUnits, 'seconds')
    rescaled.Time = rescaled.Time * 60;
  end
elseif strcmp(oldUnits, 'hours')
  if strcmp(newUnits, 'minutes')
    rescaled.Time = rescaled.Time * 60;
  elseif strcmp(newUnits, 'seconds')
    rescaled.Time = rescaled.Time * 3600;
  end
end

end
