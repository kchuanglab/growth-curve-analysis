function annotated = annotateGrowthRatesWithMaxGrowthRate(growthRates, growthRateUpperCeiling, n)
%annotateGrowthRates Annotates the input growth rate series with events.
%  Arguments:
%    growthRates: growth rate timeseries to be annotated.
%    growthRateUpperCeiling: see findMaxGrowthRate. Optional - if not
%                            specified, no annotations for max growth rate
%                            will be added.
%    n: an ordinal. optional. If specified, annotates the first n max
%       growth rates (i.e. the highest, second-highest, etc.)
annotated = growthRates;

%% Growth rate metrics
if nargin < 3
  n = 1;
end
for i = 1:n
  [maxGrowthTime, maxGrowthRate] = findMaxGrowthRate(growthRates, growthRateUpperCeiling, i);
  if i == 0
    annotated = addevent(annotated, ['Max Growth Rate = ' maxGrowthRate], maxGrowthTime);
  else
    annotated = addevent(annotated, ['Max Growth Rate ' num2str(i) ' = ' maxGrowthRate], maxGrowthTime);
  end
end

end
