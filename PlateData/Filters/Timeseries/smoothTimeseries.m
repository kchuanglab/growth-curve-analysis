function smoothed = smoothTimeseries(raw, smoothingPasses, smoothingSpan)
%smoothTimeseries Smooths data.
%  Arguments:
%    raw: timeseries to be smoothed
%    smoothingPasses: the number of times to smooth the growth rate
%                     constants.
%    smoothingSpan: the smoothing span.
smoothed = raw;
if nargin > 2
  for pass = 1:smoothingPasses
    smoothed.Data = smooth(smoothed.Data, smoothingSpan);
  end
end
set(smoothed, 'Data', smooth(raw.Data));
smoothed.UserData.Smoothing = 'moving';
smoothed.UserData.SmoothingPasses = smoothingPasses;
smoothed.UserData.SmoothingSpan = smoothingSpan;

end