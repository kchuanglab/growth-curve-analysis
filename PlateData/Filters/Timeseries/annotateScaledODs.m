function annotated = annotateScaledODs(scaledODs, props, dataSet)
%annotateScaledODs Annotates the input scaled OD series with events,
%                  according to the specified properties struct
%  Arguments:
%    scaledODs: scaled OD series to be annotated.
%     props: a properties struct specifying optional parameters for
%            adding annotations:
%            Timeseries
%              Annotations
%                LagTime
%                MaxGrowthRate
%                  NumberOfMaxGrowthRates
%     dataSet: a DataSet
annotated = scaledODs;

annotateLagTime = isfield(props, 'Timeseries') && isfield(props.Timeseries, 'Annotations') && isfield(props.Timeseries.Annotations, 'LagTime') && props.Timeseries.Annotations.LagTime;
annotateMaxGrowthRate = isfield(props, 'Timeseries') && isfield(props.Timeseries, 'Annotations') && isfield(props.Timeseries.Annotations, 'MaxGrowthRate') && (isstruct(props.Timeseries.Annotations.MaxGrowthRate) || props.Timeseries.Annotations.MaxGrowthRate);

if annotateLagTime
  annotated = annotateScaledODsWithLagTime(annotated);
end
if annotateMaxGrowthRate
  if isfield(props.Timeseries.Annotations.MaxGrowthRate, 'NumberOfMaxGrowthRates')
    annotated = annotateScaledODsWithMaxGrowthRate(annotated, dataSet.growthRateUpperCeiling, props.Timeseries.Annotations.MaxGrowthRate.NumberOfMaxGrowthRates);
  else
    annotated = annotateScaledODsWithMaxGrowthRate(annotated, dataSet.growthRateUpperCeiling);
  end
end

end
