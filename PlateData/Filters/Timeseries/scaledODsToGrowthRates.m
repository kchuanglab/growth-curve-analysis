function growthRates = scaledODsToGrowthRates(scaledODs)
%scaledODsToGrowthRates Calculates the growth rate series for the input scaled OD series.
%  Arguments:
%    scaledODs: OD values, scaled against the minimum OD value of the series.
%logDifferences = diff(log(scaledODs.Data));

scaledODs = rescaleTime(scaledODs, 'hours');

logDifferences = diff(scaledODs.Data);
timeDifferences = diff(scaledODs.Time);
growthRates = timeseries(logDifferences ./ timeDifferences, scaledODs.Time(2:end));
growthRates.TimeInfo.Units = scaledODs.TimeInfo.Units;
growthRates.DataInfo.Units = [scaledODs.DataInfo.Units ' per hour'];
growthRates.UserData = scaledODs.UserData;

end
