DataSetMap
=========

Functions in this directory:

  - Act on a string->DataSet Map. Keys should be the names of the DataSets.
  - Do not modify inputs (this is important because Map is a handle class).