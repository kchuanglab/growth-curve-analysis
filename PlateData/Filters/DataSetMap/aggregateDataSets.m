function aggregates = aggregateDataSets(dataSets, aggregationProperty)
%aggregateDataSets Aggregates input data sets by the specified property
%                  of a Sample, e.g. 'media'
%  Arguments:
%    dataSets: the DataSets to aggregate
%    aggregationProperty: a dot-delimited field path to a field in the
%                         properties struct of a Sample.
aggregates = containers.Map();
dataSetsCopy = containers.Map(keys(dataSets), values(dataSets, keys(dataSets)));
% As long as there are dataSets, keep forming additional AggregateSampleDataSets
while ~isempty(keys(dataSetsCopy))
  currKeys = keys(dataSetsCopy);
  aggregatePrototype = dataSetsCopy(currKeys{1}).representativeSample();
  % Search for more data sets with the same value for the
  % aggregationProperty property as the aggregate prototype; compile
  % a cell array of keys of these dataSets
  aggregateKeys = {};
  for key = currKeys
    currSample = dataSetsCopy(key{:}).representativeSample();
    aggregatePrototypeValue = getStructValue(aggregatePrototype.props, aggregationProperty);
    currSampleValue = getStructValue(currSample.props, aggregationProperty);
    if isstruct(aggregatePrototypeValue) && isstruct(currSampleValue)
      if structeq(aggregatePrototypeValue, currSampleValue)
        aggregateKeys = [aggregateKeys key{:}];
      end
    elseif ischar(aggregatePrototypeValue) && ischar(currSampleValue)
      if strcmp(aggregatePrototypeValue, currSampleValue)
        aggregateKeys = [aggregateKeys key{:}];
      end
    elseif aggregatePrototypeValue == currSampleValue
      aggregateKeys = [aggregateKeys key{:}];
    end
  end
  % Use the key set
  aggregateValues = values(dataSetsCopy, aggregateKeys);
  aggregateName = getStructValue(aggregatePrototype.props, aggregationProperty);
  if isstruct(aggregateName)
    aggregateName = struct2str(aggregateName);
  elseif isnumeric(aggregateName)
    aggregateName = num2str(aggregateName);
  elseif islogical(aggregateName)
    if aggregateName
      aggregateName = 'true';
    else
      aggregateName = 'false';
    end
  elseif ~ischar(aggregateName)
    aggregateName = char(aggregateName);
  end
  aggregates(aggregateName) = AggregateSampleDataSet(containers.Map(aggregateKeys, aggregateValues), aggregateName);
  remove(dataSetsCopy, aggregateKeys);
end

end