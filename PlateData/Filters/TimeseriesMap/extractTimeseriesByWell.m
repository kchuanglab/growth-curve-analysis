function extracted = extractTimeseriesByWell(all, wells, excludedWells)
%extractTimeseriesByWell Extracts the timeseries corresponding to the
%                        specified wells.
%  Arguments:
%    all: all timeseries on a plate
%    wells: an array of Wells, specifying which timeseries to extract
%    excludedWells: an array of Wells whose data should be discarded

extracted = containers.Map();
for well = wells
  if strcmp(well.name, excludedWells) == 0
    extracted(well.name) = all(well.name);
  else
    disp(['Skipped well ' well.name])
  end
end

end