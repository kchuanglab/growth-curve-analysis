TimeseriesMap
=========

Functions in this directory:

  - Act on a string->timeseries Map. Keys should be the names of the timeseries, which should be well names.
  - Do not modify inputs (this is important because Map is a handle class).