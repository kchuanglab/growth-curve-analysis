function saveImage(imageHandle, axesHandle, props, figureHandle)
%saveFigure Saves an image according to the specified properties struct
%  Arguments:
%    imageHandle: handle to the figure containing the image to save
%    axesHandle: handle to the axes containing the image to save
%    props: a properties struct specifying optional parameters for saving:
%           Saving
%             Prefix
%             Filename (this is required if Saving is present)
%             Suffix
%             Extension
%             AlsoSaveAsFig
%    figureHandle: handle to the figure from which the image was
%                  generated. Optional - only used when AlsoSaveAsFig is
%                  true.

if isfield(props, 'Saving')
  props = props.Saving;
  targetName = props.Filename;
  if isfield(props, 'Prefix')
    targetName = [props.Prefix targetName];
  end
  if isfield(props, 'Suffix')
    targetName = [targetName props.Suffix];
  end
  if ~isfield(props, 'Extension')
    props.Extension = 'png';
  end
  imwrite(getfield(getframe(axesHandle), 'cdata'), [targetName '.' props.Extension]);
  display(['Saved to ' targetName '.' props.Extension]);
  if isfield(props, 'AlsoSaveAsFig') && props.AlsoSaveAsFig && ~strcmp(props.Extension, 'fig')
    saveas(figureHandle, [targetName '.fig']);
    display(['Also saved to ' targetName '.fig']);
  end
  close(imageHandle);
end

end
