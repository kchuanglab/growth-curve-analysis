function setTitle(axesHandle, props, titleName)
%setTitle Sets the title of a set of axes, according to the specified
%         properties struct
%   Arguments:
%     axesHandle: handle to the axes for which the title should be set
%     props: a properties struct specifying optional parameters for
%            adding the title:
%            Figure
%              Title
%                Size
%                Color
%     titleName: the title to set

if isfield(props, 'Figure') && isfield(props.Figure, 'Title')
  props = props.Figure.Title;
end

titleHandle = title(axesHandle, titleName);
if isfield(props, 'Size')
  set(titleHandle, 'FontSize', props.Size);
end
if isfield(props, 'Color')
  set(titleHandle, 'Color', props.Color);
end

end
