function figureHandle = makeFigure(props)
%makeFigure Makes a figure according to the specified properties struct
%   Arguments:
%     props: a properties struct specifying parameters for making the figure.
%            Figure
%              Position

figureHandle = figure;

if isfield(props, 'Figure')
  props = props.Figure;
end

if isfield(props, 'Position')
  set(figureHandle, 'Position', props.Position);
end

end
