function setLegend(axesHandle, props, legendNames, plotHandles)
%setTitle Sets the title of a set of axes, according to the specified
%         properties struct
%   Arguments:
%     axesHandle: handle to the axes to which the legend should be added
%     props: a properties struct specifying optional parameters for
%            adding the legend:
%            Figure
%              Legend
%                Size
%     titleName: the entries of the legend to add
%     plotHandles: a vector of handles to plots; optional - does not apply
%                  for plots of timeseries, and only useful when plotting
%                  properties such as color are set with the set function
%                  instead of set by the plotter (for example, with the
%                  errorbars function)

if isfield(props, 'Figure') && isfield(props.Figure, 'Legend')
  props = props.Figure.Legend;
end

if nargin < 4
  legendHandle = legend(axesHandle, legendNames);
else
  legendHandle = legend(axesHandle, plotHandles, legendNames);
end

if isfield(props, 'Size')
  set(legendHandle, 'FontSize', props.Size);
end

end
