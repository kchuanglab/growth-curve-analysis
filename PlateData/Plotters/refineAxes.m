function refineAxes(axesHandle, props, xAxisName, yAxisName, xTickLabels)
%refineAxes Sets properties of a set of axes, along with its associated
%           labels, according to the specified properties struct
%   Arguments:
%     axesHandle: handle to the axes containing the image to save
%     props: a properties struct specifying optional parameters for
%            adjusting axes:
%           Axes
%             Limits
%               X
%               Y
%             Labels
%               XSize
%               YSize
%               XColor
%               YColor
%             Ticks
%               LabelSize
%               XLabelRotation
%     xAxisName: name of the x axis
%     yAxisName: name of the y axis
%     xTickLabels: tick labels; optional - only used if the tick labels
%                  must be rotated.

if isfield(props, 'Axes')
  props = props.Axes;
end

% Axis limits
if isfield(props, 'Limits')
  if isfield(props.Limits, 'X')
    xlim(axesHandle, props.Limits.X);
  end
  if isfield(props.Limits, 'Y')
    ylim(axesHandle, props.Limits.Y);
  end
end

% Axis labels
xLabelHandle = xlabel(axesHandle, xAxisName);
yLabelHandle = ylabel(axesHandle, yAxisName);
if isfield(props, 'Labels')
  if isfield(props.Labels, 'XSize')
    set(xLabelHandle, 'FontSize', props.Labels.XSize);
  end
  if isfield(props.Labels, 'YSize')
    set(yLabelHandle, 'FontSize', props.Labels.YSize);
  end
  if isfield(props.Labels, 'XColor')
    set(xLabelHandle, 'Color', props.Labels.XColor);
  end
  if isfield(props.Labels, 'YColor')
    set(yLabelHandle, 'Color', props.Labels.YColor);
  end
end

% Axis ticks
if isfield(props, 'Ticks')
  if isfield(props.Ticks, 'LabelSize')
    set(axesHandle, 'FontSize', props.Ticks.LabelSize);
  end
  if isfield(props.Ticks, 'XLabelRotation')
    xticklabel_rotate(1:numel(xTickLabels), props.Ticks.XLabelRotation, xTickLabels);
  end
end

end
