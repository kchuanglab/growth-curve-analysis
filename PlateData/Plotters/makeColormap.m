function cmap = makeColormap(props, numColors)
%makeColormap Outputs a colormap (for use by MATLAB's colormap function)
%             according to the specified value or properties struct
%   Arguments:
%     props: an m-by-3 colormap matrix, or a string that gives the name of
%            a built-in colormap, or a properties struct that specifies how
%            to generate the colormap using cbrewer.
%            Required (if struct): Type, Name
%     numColors: an integer specifying the number of colors that
%                should be in the colormap. The number of colors in the
%                colormap never exceeds the maximum number of colors in the
%                color brewer scheme, however. Optional - only used when
%                props is a struct for generation of a color brewer
%                colormap.

if isstruct(props)
  if strcmp(props.Type, 'div')
    numColors = min(11, numColors);
  elseif strcmp(props.Type, 'seq')
    numColors = min(9, numColors);
  else
    if strcmp(props.Name, 'Set3') || strcmp(props.Name, 'Paired')
      numColors = min(12, numColors);
    elseif strcmp(props.Name, 'Set1') || strcmp(props.Name, 'Pastel1')
      numColors = min(9, numColors);
    else
      numColors = min(8, numColors);
    end
  end
  cmap = cbrewer(props.Type, props.Name, numColors);
else
  cmap = props;
end

end
