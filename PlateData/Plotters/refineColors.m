function refineColors(figureHandle, axesHandle, props, numColors)
%refineColors Sets colors of the figure, according to the specified
%             properties struct
%   Arguments:
%     figureHandle: handle to the figure to adjust
%     axesHandle: handle to the axes to adjust
%     props: a properties struct specifying optional parameters for
%            colors:
%            Color
%              Colormap
%              Background
%     numColors: the number of colors that should be in the colormap.
%                Optional - only used for generating new colormaps using
%                cbrewer.

if isfield(props, 'Color')
  props = props.Color;
end

% Background color
if isfield(props, 'Background')
  whitebg(figureHandle, props.Background);
end

% Line color
if isfield(props, 'Colormap')
  cmapline('ax', axesHandle, 'colormap', makeColormap(props.Colormap, numColors));
end

end
