function h = plotRawODTimeseries(sds, props)
%plotRawODTimeseries Plots the raw OD timeseries data of the input
%                    SampleDataSet.
%  Arguments:
%    sds: SampleDataSet (or a map of SampleDataSets) to be plotted.
%    props: a properties struct specifying optional parameters for figure
%           generation:
%           Figure
%             Position
%             Title
%               Size
%               Color
%             Legend
%               Size
%             Antialiasing
%               Supersampling
%           Axes
%             Limits
%               X
%               Y
%             Labels
%               XSize
%               YSize
%               XColor
%               YColor
%             Ticks
%               LabelSize
%               XLabelRotation
%           Lineseries
%             Lines
%               Style
%               Width
%             Markers
%               Marker
%               EdgeColor
%               FaceColor
%               Size
%           Timeseries
%             Rescale
%           Color
%             Colormap
%             Background
%           Saving
%             Prefix
%             Filename
%             Suffix
%             Extension
%             AlsoSaveAsFig

if isa(sds, 'containers.Map')
  for actualSDS = values(sds)
    plotRawODTimeseries(actualSDS{:}, props);
  end
else
  %% Process special flags in props
  rescale = isfield(props, 'Timeseries') && isfield(props.Timeseries, 'Rescale') && props.Timeseries.Rescale;
  
  %% Prepare data
  rawODTimeseries = sds.odTimeseries;
  rawODTimeseriesKeys = keys(rawODTimeseries);
  
  %% Set names for labels
  legendNames = rawODTimeseriesKeys;
  titleName = ['Raw Growth Curves, Split by Well: ' sds.fullName];
  yUnits = rawODTimeseries(rawODTimeseriesKeys{1}).DataInfo.Units;
  yAxisName = strcat('OD (', yUnits, ')');
  if rescale
    xUnits = 'hours';
  else
    xUnits = seriesAverage.TimeInfo.Units;
  end
  xAxisName = strcat('Time (', xUnits, ')');
  
  %% Set up figure
  h = makeFigure(props);
  hold all;
  
  %% Plot data
  if ~isfield(props, 'Lineseries')
    props.Lineseries = struct();
  end
  plottingProperties = extractPlottingProperties(props.Lineseries);
  % Plot the raw OD timeseries of each individual well in the DataSet
  for key = rawODTimeseriesKeys
    % Retrieve current timeseries
    current = rawODTimeseries(key{:});
    if rescale
      current = rescaleTime(current, 'hours');
    end
    % Plot timeseries
    plot(current, plottingProperties{:});
  end
  
  %% Finish plotting
  hold off;
  refineColors(h, gca, props, numel(legendNames));
  % Set labels
  setTitle(gca, props, titleName);
  refineAxes(gca, props, xAxisName, yAxisName);
  setLegend(gca, props, legendNames);
  renderAndSave(h, props);
end

end
