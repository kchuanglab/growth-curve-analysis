function h = plotMaxGrowthRates(ds, props, splitting)
%plotMaxGrowthRates Plots the max growth rate data of the input DataSet.
%  Arguments:
%    ds: DataSet (or a Map of DataSets) to be plotted.
%    props: a properties struct specifying optional parameters for figure
%           generation:
%           Figure
%             Position
%             Title
%               Size
%               Color
%             Antialiasing
%               Supersampling
%           Axes
%             Limits
%               X
%               Y
%             Labels
%               XSize
%               YSize
%               XColor
%               YColor
%             Ticks
%               LabelSize
%               XLabelRotation
%           Processing
%             Smoothing
%           Timeseries
%             Averaging
%           Color
%             Background
%           Saving
%             Prefix
%             Filename
%             Suffix
%             Extension
%             AlsoSaveAsFig
%    Splitting: whether to plot one bar per constituent Well, or one bar
%               per constituent DataSet. Optional - only used when the
%               input DataSet is an AggregateSampleDataSet. Either 'Well'
%               or 'DataSet'.

if isa(ds, 'containers.Map')
  for actualDS = values(ds)
    if nargin < 3
      plotMaxGrowthRates(actualDS{:}, props);
    else
      plotMaxGrowthRates(actualDS{:}, props, splitting);
    end
  end
else
  %% Process special flags in props
  smoothing = isfield(props, 'Processing') && isfield(props.Processing, 'Smoothing') && props.Processing.Smoothing;
  averaging = isfield(props, 'Timeseries') && isfield(props.Timeseries, 'Averaging') && (isstruct(props.Timeseries.Averaging) || props.Timeseries.Averaging);
  
  %% Prepare data
  if nargin > 2 && strcmpi(splitting, 'DataSet') && metaclass(ds) <= ?AggregateSampleDataSet
    maxGrowthRatesMap = ds.dataSetMaxGrowthRates(smoothing);
    splitting = 'DataSet';
  else
    maxGrowthRatesMap = ds.wellMaxGrowthRates(smoothing);
    splitting = 'Well';
  end
  maxGrowthRates = cell2mat(values(maxGrowthRatesMap));
  maxGrowthRatesKeys = keys(maxGrowthRatesMap);
  averageGrowthRateTimeseries = ds.averageGrowthRateTimeseries(smoothing);
  averageMaxGrowthRate = ds.averageMaxGrowthRate(smoothing);
  
  %% Set names for labels
  xTickLabels = maxGrowthRatesKeys;
  if averaging
    xTickLabels = [xTickLabels 'Average'];
  end
  titleName = ['Max Growth Rates, Split by ' splitting ': ' ds.name];
  if smoothing
    titleName = ['Smoothed ' titleName];
  end
  yUnits = averageGrowthRateTimeseries.DataInfo.Units;
  yAxisName = strcat('Max Growth Rate (', yUnits, ')');
  xAxisName = strcat('Sample');
  
  %% Set up figure
  h = makeFigure(props);
  hold all;
  
  %% Plot data
  if averaging
    maxGrowthRates(end + 1) = averageMaxGrowthRate;
  end
  bar(maxGrowthRates);
  
  %% Finish plotting
  hold off;
  refineColors(h, gca, props);
  % Set labels
  setTitle(gca, props, titleName);
  refineAxes(gca, props, xAxisName, yAxisName, xTickLabels);
  renderAndSave(h, props);
end

end
