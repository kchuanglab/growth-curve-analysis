function h = plotGrowthRateTimeseries(ds, props, splitting)
%plotGrowthRateTimeseries Plots the growth rate timeseries of the input
%                         DataSet.
%  Arguments:
%    ds: DataSet (or a Map of DataSets) to be plotted.
%    props: a properties struct specifying optional parameters for figure
%           generation:
%           Figure
%             Position
%             Title
%               Size
%               Color
%             Legend
%               Size
%             Antialiasing
%               Supersampling
%           Axes
%             Limits
%               X
%               Y
%             Labels
%               XSize
%               YSize
%               XColor
%               YColor
%             Ticks
%               LabelSize
%               XLabelRotation
%           Processing
%             Smoothing
%             NormalizeByMaximum
%           Lineseries
%             Lines
%               Style
%               Width
%             Markers
%               Marker
%               EdgeColor
%               FaceColor
%               Size
%           Timeseries
%             Rescale
%             Averaging
%               Lines
%                 Style
%                 Width
%             Markers
%               Marker
%               EdgeColor
%               FaceColor
%               Size
%             Annotations
%               MaxGrowthRate
%                 NumberOfMaxGrowthRates
%           Color
%             Colormap
%             Background
%           Saving
%             Prefix
%             Filename
%             Suffix
%             Extension
%             AlsoSaveAsFig
%    Splitting: whether to plot one line per constituent Well, or one line
%               per constituent DataSet. Optional - only used when the
%               input DataSet is an AggregateSampleDataSet. Either 'Well'
%               or 'DataSet'.

if isa(ds, 'containers.Map')
  for actualDS = values(ds)
    if nargin < 3
      plotGrowthRateTimeseries(actualDS{:}, props);
    else
      plotGrowthRateTimeseries(actualDS{:}, props, splitting);
    end
  end
else
  %% Process special flags in props
  rescale = isfield(props, 'Timeseries') && isfield(props.Timeseries, 'Rescale') && props.Timeseries.Rescale;
  smoothing = isfield(props, 'Processing') && isfield(props.Processing, 'Smoothing') && props.Processing.Smoothing;
  averaging = isfield(props, 'Timeseries') && isfield(props.Timeseries, 'Averaging') && (isstruct(props.Timeseries.Averaging) || props.Timeseries.Averaging);
  normalization = isfield(props, 'Processing') && isfield(props.Processing, 'NormalizeByMaximum') && props.Processing.NormalizeByMaximum;
  
  %% Prepare data
  if nargin > 2 && strcmpi(splitting, 'DataSet') && metaclass(ds) <= ?AggregateSampleDataSet
    growthRateTimeseries = ds.dataSetGrowthRateTimeseries(smoothing);
    splitting = 'DataSet';
  else
    growthRateTimeseries = ds.wellGrowthRateTimeseries(smoothing);
    splitting = 'Well';
  end
  timeseriesKeys = keys(growthRateTimeseries);
  averageGrowthRateTimeseries = ds.averageGrowthRateTimeseries(smoothing);
  
  %% Set names for labels
  legendNames = timeseriesKeys;
  if averaging
    legendNames = [legendNames 'Average'];
  end
  titleName = ['Growth Rate Curves, Split by ' splitting ': ' ds.fullName];
  if smoothing
    titleName = ['Smoothed ' titleName];
  end
  yUnits = averageGrowthRateTimeseries.DataInfo.Units;
  if normalization
    yUnits = [yUnits ' (Normalized)'];
    titleName = ['Normalized ' titleName];
  end
  yAxisName = strcat('Growth Rate (', yUnits, ')');
  if rescale
    xUnits = 'hours';
  else
    xUnits = averageGrowthRateTimeseries.TimeInfo.Units;
  end
  xAxisName = strcat('Time (', xUnits, ')');
  
  %% Set up figure
  h = makeFigure(props);
  hold all;
  
  %% Plot data
  if ~isfield(props, 'Lineseries')
    props.Lineseries = struct();
  end
  plottingProperties = extractPlottingProperties(props.Lineseries);
  for key = timeseriesKeys
    current = growthRateTimeseries(key{:});
    if rescale
      current = rescaleTime(current, 'hours');
    end
    current = annotateGrowthRates(current, props, ds);
    if normalization
      [~, maxGrowthRate] = findMaxGrowthRate(current, ds.growthRateUpperCeiling);
      current = current / maxGrowthRate;
    end
    plot(current, plottingProperties{:});
  end
  % Conditionally plot average growth rate timeseries
  if averaging
    if rescale
      averageGrowthRateTimeseries = rescaleTime(averageGrowthRateTimeseries, 'hours');
    end
    if normalization
      [~, maxGrowthRate] = findMaxGrowthRate(averageGrowthRateTimeseries, ds.growthRateUpperCeiling);
      averageGrowthRateTimeseries = averageGrowthRateTimeseries / maxGrowthRate;
    end
    averagePlottingProperties = extractPlottingProperties(props.Timeseries.Averaging);
    plot(averageGrowthRateTimeseries, averagePlottingProperties{:});
  end
  
  %% Finish plotting
  hold off;
  refineColors(h, gca, props, numel(legendNames));
  % Set labels
  setTitle(gca, props, titleName);
  refineAxes(gca, props, xAxisName, yAxisName);
  setLegend(gca, props, legendNames);
  renderAndSave(h, props);
end

end
