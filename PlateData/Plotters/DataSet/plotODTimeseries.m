function h = plotODTimeseries(ds, props, splitting)
%plotODTimeseries Plots the OD timeseries of the input DataSet.
%  Arguments:
%    ds: DataSet (or a Map of DataSets) to be plotted.
%    props: a properties struct specifying optional parameters for figure
%           generation:
%           Figure
%             Position
%             Title
%               Size
%               Color
%             Legend
%               Size
%             Antialiasing
%               Supersampling
%           Axes
%             Limits
%               X
%               Y
%             Labels
%               XSize
%               YSize
%               XColor
%               YColor
%             Ticks
%               LabelSize
%               XLabelRotation
%           Processing
%             Smoothing
%           Lineseries
%             Lines
%               Style
%               Width
%             Markers
%               Marker
%               EdgeColor
%               FaceColor
%               Size
%           Timeseries
%             Rescale
%             Averaging
%               Lines
%                 Style
%                 Width
%             Markers
%               Marker
%               EdgeColor
%               FaceColor
%               Size
%             Annotations
%               MaxGrowthRate
%                 NumberOfMaxGrowthRates
%               LagTime
%           Color
%             Colormap
%             Background
%           Saving
%             Prefix
%             Filename
%             Suffix
%             Extension
%             AlsoSaveAsFig
%    Splitting: whether to plot one line per constituent Well, or one line
%               per constituent DataSet. Optional - only used when the
%               input DataSet is an AggregateSampleDataSet. Either 'Well'
%               or 'DataSet'.

if isa(ds, 'containers.Map')
  for actualDS = values(ds)
    if nargin < 3
      plotODTimeseries(actualDS{:}, props);
    else
      plotODTimeseries(actualDS{:}, props, splitting);
    end
  end
else
  %% Process special flags in props
  rescale = isfield(props, 'Timeseries') && isfield(props.Timeseries, 'Rescale') && props.Timeseries.Rescale;
  smoothing = isfield(props, 'Processing') && isfield(props.Processing, 'Smoothing') && props.Processing.Smoothing;
  averaging = isfield(props, 'Timeseries') && isfield(props.Timeseries, 'Averaging') && (isstruct(props.Timeseries.Averaging) || props.Timeseries.Averaging);
  
  %% Prepare data
  if nargin > 2 && strcmpi(splitting, 'DataSet') && metaclass(ds) <= ?AggregateSampleDataSet
    odTimeseries = ds.dataSetODTimeseries(smoothing);
    splitting = 'DataSet';
  else
    odTimeseries = ds.wellODTimeseries(smoothing);
    splitting = 'Well';
  end
  timeseriesKeys = keys(odTimeseries);
  averageODTimeseries = ds.averageODTimeseries(smoothing);
  
  %% Set names for labels
  legendNames = timeseriesKeys;
  if averaging
    legendNames = [legendNames 'Average'];
  end
  titleName = ['Scaled Growth Curves, Split by ' splitting ': ' ds.name];
  if smoothing
    titleName = ['Smoothed ' titleName];
  end
  yUnits = averageODTimeseries.DataInfo.Units;
  yAxisName = strcat('Scaled OD (', yUnits, ')');
  if rescale
    xUnits = 'hours';
  else
    xUnits = averageODTimeseries.TimeInfo.Units;
  end
  xAxisName = strcat('Time (', xUnits, ')');
  
  %% Set up figure
  h = makeFigure(props);
  hold all;
  
  %% Plot data
  if ~isfield(props, 'Lineseries')
    props.Lineseries = struct();
  end
  plottingProperties = extractPlottingProperties(props.Lineseries);
  for key = timeseriesKeys
    current = odTimeseries(key{:});
    if rescale
      current = rescaleTime(current, 'hours');
    end
    current = annotateScaledODs(current, props, ds);
    plot(current, plottingProperties{:});
  end
  % Conditionally plot average OD timeseries
  if averaging
    if rescale
      averageODTimeseries = rescaleTime(averageODTimeseries, 'hours');
    end
    averagePlottingProperties = extractPlottingProperties(props.Timeseries.Averaging);
    plot(averageODTimeseries, averagePlottingProperties{:});
  end
  
  %% Finish plotting
  hold off;
  refineColors(h, gca, props, numel(legendNames));
  % Set labels
  setTitle(gca, props, titleName);
  refineAxes(gca, props, xAxisName, yAxisName);
  setLegend(gca, props, legendNames);
  renderAndSave(h, props);
end

end
