function renderAndSave(figureHandle, props)
%renderAndSave Conditionally makes an antialiased version of the current
%              figure according to the specified properties struct, and
%              then conditionally saves figure
%  Arguments:
%    figureHandle: handle to the figure to save
%    props: a properties struct specifying parameters for making the figure.
%           Figure
%             Antialiasing
%               Supersampling
%           Saving
%             Prefix
%             Filename
%             Suffix
%             Extension
%             AlsoSaveAsFig

if isfield(props, 'Saving') && ~isstruct(props.Saving)
  props.Saving = struct();
end
if isfield(props, 'Saving') && ~isfield(props.Saving, 'Filename')
  props.Saving.Filename = regexprep(get(get(gca, 'Title'), 'String'), '[/:*?"<>|\\-]', '_');
end

if isfield(props, 'Figure') && isfield(props.Figure, 'Antialiasing')
  if isfield(props.Figure.Antialiasing, 'Supersampling')
    antialiasedFigureHandle = myaa(props.Figure.Antialiasing.Supersampling);
  else
    antialiasedFigureHandle = myaa();
  end
  saveImage(antialiasedFigureHandle, gca, props, figureHandle);
  close(figureHandle);
else
  saveFigure(figureHandle, props);
end

end
