function plottingProperties = extractPlottingProperties(props)
%plotTimeseries Plots a timeseries, according to the specified properties
%               struct
%   Arguments:
%     timeseries: the timeseries to plot
%     props: a properties struct specifying optional parameters for
%            plotting the timeseries. Optional - if not provided, default
%            parameters from MATLAB are used.
%            Lines
%              Style
%              Width
%            Markers
%              Marker
%              EdgeColor
%              FaceColor
%              Size

if ~isstruct(props)
  props = struct();
end
plottingProperties = {};

if isfield(props, 'Lines')
  if isfield(props.Lines, 'Style')
    plottingProperties = [plottingProperties 'LineStyle' props.Lines.Style];
  end
  if isfield(props.Lines, 'Width')
    plottingProperties = [plottingProperties 'LineWidth' props.Lines.Width];
  end
end
if isfield(props, 'Markers')
  if isfield(props.Markers, 'Marker')
    plottingProperties = [plottingProperties 'Marker' props.Markers.Marker];
  end
  if isfield(props.Markers, 'EdgeColor')
    plottingProperties = [plottingProperties 'MarkerEdgeColor' props.Markers.EdgeColor];
  end
  if isfield(props.Markers, 'FaceColor')
    plottingProperties = [plottingProperties 'MarkerFaceColor' props.Markers.FaceColor];
  end
  if isfield(props.Markers, 'Size')
    plottingProperties = [plottingProperties 'MarkerSize' props.Markers.Size];
  end
end

end
