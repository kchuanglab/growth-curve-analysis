function saveFigure(figureHandle, props)
%saveFigure Saves a figure according to the specified properties struct
%  Arguments:
%    figureHandle: handle to the figure to save
%    props: a properties struct specifying optional parameters for saving:
%           Saving
%             Prefix
%             Filename (this is required if Saving is present)
%             Suffix
%             Extension
%             AlsoSaveAsFig

if isfield(props, 'Saving')
  props = props.Saving;
  targetName = props.Filename;
  if isfield(props, 'Prefix')
    targetName = [props.Prefix targetName];
  end
  if isfield(props, 'Suffix')
    targetName = [targetName props.Suffix];
  end
  if ~isfield(props, 'Extension')
    props.Extension = 'fig';
  end
  saveas(figureHandle, [targetName '.' props.Extension]);
  display(['Saved to ' targetName '.' props.Extension]);
  if isfield(props, 'AlsoSaveAsFig') && props.AlsoSaveAsFig && ~strcmp(props.Extension, 'fig')
    saveas(figureHandle, [targetName '.fig']);
    display(['Also saved to ' targetName '.fig']);
  end
  close(figureHandle);
end

end
