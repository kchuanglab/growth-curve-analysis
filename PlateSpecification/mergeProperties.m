function merged = mergeProperties(props1, props2, uninitialized)
%mergeProperties Recursively erges two properties structs; values which are
%                different are initialized to the uninitialized property,
%                as specified by the uninitialized properties struct.
%   Arguments:
%     props1: a properties struct
%     props2: a second properties struct
%     uninitialized: a properties struct whose values are all uninitialized

merged = props1;

for field = fieldnames(props1)'
  if ~isfield(props2, field{:})
    error(struct('Message', 'The props2 struct does not have exactly the same fields as the props1 struct!', 'props1', props1, 'props2', props2))
  elseif isstruct(props1.(field{:}))
    if ~isstruct(props2.(field{:}))
      error(struct('Message', 'The props2 struct does not have exactly the same fields as the props1 struct!', 'props1', props1, 'props2', props2))
    elseif ~isstruct(uninitialized.(field{:}))
      error(struct('Message', 'The uninitialized struct does not have exactly the same fields as the props1 struct!', 'props1', props1, 'uninitialized', uninitialized))
    else
      merged.(field{:}) = mergeProperties(props1.(field{:}), props2.(field{:}), uninitialized.(field{:}));
    end
  elseif ~isequal(props1.(field{:}), props2.(field{:}))
    if ~isfield(uninitialized, field{:})
      error(struct('Message', 'The uninitialized struct does not have exactly the same fields as the props1 struct!', 'props1', props1, 'uninitialized', uninitialized))
    else
      merged.(field{:}) = uninitialized.(field{:});
    end
  end
end

end
