function props = generatePropsFromMap(propertiesMap, uninitialized, orderedFields)
%generatePropsFromMap Generates a properties struct from a Map.
% propertiesMap should be a Map in which the keys are the dot-delimited
%               field paths of the properties struct (e.g. "foo", or
%               "foo.bar.quux"), and the values are the values that should
%               be assigned to the corresponding fields in the properties
%               struct.
% uninitialized should be a boolean that specifies whether the resulting
%               properties struct should have all its values as an
%               uninitialized value, instead of using the values from
%               propertiesMap. Optional - if not provided, it will be set
%               to "false", i.e. not uninitialized
% orderedfields should be a cell array that lists the properties fields
%               in a custom order. Only relevant when "uninitialized" is
%               false. Optional - if not provided, fields will be listed
%               alphabetically in the properties struct.
props = struct();

if nargin < 2
  uninitialized = false;
end
if nargin < 3
  orderedFields = keys(propertiesMap);
end

for key = orderedFields
  if ~uninitialized
    value = propertiesMap(key{:});
  else
    value = @uninitializedValue;
  end
  props = setStructValue(props, key{:}, value);
end

end

function uninitializedValue
end