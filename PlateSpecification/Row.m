classdef Row
  %Row Enumeration to specify a row of a 96-well plate
  %   Options: A, B, C, D, E, F, G, H
  
  enumeration
    A, B, C, D, E, F, G, H
  end
  
end

