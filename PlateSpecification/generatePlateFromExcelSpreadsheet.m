function plate = generatePlateFromExcelSpreadsheet(spreadsheetPath, plateRows, plateCols)
%generatePlateFromExcelSpreadsheet Generates a Plate object, given an excel
%                                  spreadsheet which lays out how Sample
%                                  properties are distributed across the
%                                  plate.
%   Arguments:
%     spreadsheetPath: path to the spreadsheet to use. Plate layout
%                      information must be in the first worksheet.
%     plateRows: the number of listed rows on the plate. For example, if
%                only the first 4 rows of the 8-row plate have data, you
%                can omit the last 4 rows from your layout (including the
%                row labels), and specify 4 for plateRows. Alternately, you
%                can simply leave the row labels for the last 4 rows in
%                place and leave the blank the cells corresponding to the
%                empty wells, and the same behavior will result; this is
%                probably better for consistency.
%     plateColumns: the number of listed columns on the plate

[~, ~, raw] = xlsread(spreadsheetPath, 1');

% Determine the indices upper-left corners on which plate layouts start
% (each upper-left corner is marked by an asterisk)
[cornerRows, cornerCols] = find(strcmp('*', raw));

sampleWells = containers.Map;
sampleProps = containers.Map;
sampleUninitializedProps = containers.Map;
% Iterate through each well
for wellRow = 1:plateRows
  for wellCol = 1:plateCols
    propsMap = containers.Map;
    orderedFields = {};
    % Iterate through each property by iterating through each upper-left
    % corner
    for corner = 1:length(cornerRows)
      currentProperty = raw{cornerRows(corner), cornerCols(corner) - 1};
      value = raw{cornerRows(corner) + wellRow, cornerCols(corner) + wellCol};
      if ~isnan(value)
        propsMap(currentProperty) = value;
        if ~isempty(orderedFields)
          orderedFields = {orderedFields currentProperty};
        else
          orderedFields = currentProperty;
        end
      end
    end
    props = generatePropsFromMap(propsMap, false, orderedFields);
    % Discard wells that are associated with no properties at all, since
    % they indicate that the well was not measured.
    if ~isempty(fieldnames(props))
      propsString = struct2str(props);
      
      wellRowName = raw{cornerRows(corner) + wellRow, cornerCols(corner)};
      wellColName = raw{cornerRows(corner), cornerCols(corner) + wellCol};
      
      if isKey(sampleWells, propsString)
        previousWells = sampleWells(propsString);
        currentWells = [previousWells Well(Row.(wellRowName), wellColName)];
      else
        currentWells = Well(Row.(wellRowName), wellColName);
      end
      sampleWells(propsString) = currentWells;
      sampleProps(propsString) = props;
      sampleUninitializedProps(propsString) = generatePropsFromMap(propsMap, true);
    end
  end
end

samples = [];
for key = keys(sampleWells)
  samples = [samples; Sample(sampleWells(key{:}), sampleProps(key{:}), sampleUninitializedProps(key{:}))];
end

plate = Plate(samples);

end

