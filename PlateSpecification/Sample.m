classdef Sample
  %Sample Represents a replicates specification for a given strain/medium combination.
  %   Constructor arguments:
  %     wells: an array of Well objects representing wells.
  %     props: a properties struct.
  %     uninitialized: a version of the properties struct in which all
  %                    values are at their uninitialized values.
  %     naming: a function handle that takes props as its only argument and
  %             generates a string name from it. Optional - if not
  %             supplied, it will use a default naming function.
  %     uninitializedNaming: a function handle that takes props as its
  %                          first argument and uninitialized as its second
  %                          argument and generates a string name from the
  %                          initialized properties. Optional - if not
  %                          supplied, it will use the same function as
  %                          naming.
  
  properties
    wells = [];
    props
    naming
    uninitialized
    initializedNaming
  end
  
  properties (Dependent = true, SetAccess = private)
    name
    initializedName
  end
  
  methods
    % Constructor
    function sample = Sample(wells, props, uninitialized, naming, initializedNaming)
      %% Pre-process arguments
      if nargin < 3
        error(['Only ' num2str(nargin) ' arguments were provided'])
      end
      if nargin < 4
        naming = @struct2str;
      end
      if nargin < 5
        initializedNaming = @(props, uninitialized) naming(props);
      end
      
      %% Initialize local properties
      sample.props = props;
      sample.naming = naming;
      sample.uninitialized = uninitialized;
      sample.initializedNaming = initializedNaming;
      % Make sure wells is a horizontal array
      if size(wells, 2) == 1
        sample.wells = wells.';
      else
        sample.wells = wells;
      end
    end
    % name accessor
    function name = get.name(sample)
      name = sample.naming(sample.props);
    end
    % initializedName accessor
    function name = get.initializedName(sample)
      name = sample.initializedNaming(sample.props, sample.uninitialized);
    end
    % == operator
    function equality = eq(sampleOne, sampleTwo)
      equality = sampleOne.name == sampleTwo.name...
        & sampleOne.wells == sampleTwo.wells;
    end
  end
  
end
