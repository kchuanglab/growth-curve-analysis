function initialized = isPartiallyInitialized(props, uninitialized)
%isPartiallyInitialized Recursively checks whether the input properties
%                       struct is at least partially initialized.
%   Arguments:
%     props: a properties struct
%     uninitialized: a properties struct whose values are all uninitialized

initialized = false;

for field = fieldnames(props)
  if ~isfield(uninitialized, field{1})
    error(struct('Message', 'The uninitialized struct does not have exactly the same fields as the properties struct!', 'Properties', props, 'Uninitialized', uninitialized))
  elseif isstruct(props.(field{1}))
    if ~isstruct(uninitialized.(field{1}))
      error(struct('Message', 'The uninitialized struct does not have exactly the same fields as the properties struct!', 'Properties', props, 'Uninitialized', uninitialized))
    else
      initialized = isPartiallyInitialized(props.(field{1}), uninitialized.(field{1}));
    end
  elseif props.(field{1}) ~= uninitialized.(field{1})
    initialized = true;
  end
  if initialized
    break;
  end
end

end
