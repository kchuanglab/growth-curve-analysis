classdef Well
  %Well Represents a well on a 96-well plate.
  %   Constructor arguments:
  %     row: a Row enum value, e.g. Row.H
  %     col: a number, from 1 to 12
  %   Alternate constructor arguments:
  %     name: the string name of the well, e.g. "H12"
  
  properties
    row = 'A'
    col = 1
  end
  
  properties (Dependent = true, SetAccess = private)
    name
  end
  
  methods
    % Constructor
    function well = Well(arg1, arg2)
      switch nargin
        case 1
          well.row = char(Row.(arg1(1)));
          well.col = str2num(arg1(2:end));
        case 2
          well.row = arg1;
          well.col = arg2;
      end
    end
    % name accessor
    function name = get.name(well)
      name = strcat(well.row, int2str(well.col));
    end
    % == operator
    function equality = eq(wellOne, wellTwo)
      equality = wellOne.row == wellTwo.row...
        & wellOne.col == wellTwo.col;
    end
  end
  
end
