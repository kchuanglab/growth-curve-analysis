classdef Plate
  %Plate Represents the samples specification for a given 96-well plate.
  %   Constructor arguments:
  %     samples: a horizontal array of Sample objects
  
  properties
    samples = [];
  end
  
  methods
    % Constructor
    function plate = Plate(samples)
      if nargin > 0
        % Make sure samples is a horizontal array
        if size(samples, 2) == 1
          plate.samples = samples.';
        else
          plate.samples = samples;
        end
      end
    end
  end
  
end

