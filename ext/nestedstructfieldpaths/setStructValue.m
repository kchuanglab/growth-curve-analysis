function newObj = setStructValue(obj, fieldPath, value)
%setStructValue Sets the value of the (potentially nested) field in the
%               input struct to the specified value
% obj should be a struct
% fieldPath should be a dot-delimited string indicating the full path of
%           the field
% value should be the new value at fieldPath
%
% Example: given foo = struct('bar', struct('baz', 'foobar')),
%          setStructValue(foo, 'bar.baz', 'qux') will return the
%          equivalent of struct('bar', struct('baz', 'quz'))

% This is adapted from Bruno Luong's SetProp function, first posted at
% http://www.mathworks.com/matlabcentral/newsreader/view_original/405062
% from <brunoluong@yahoo.com> at comp.soft-sys.matlab

% Ethan Li
% lietk12@gmail.com
% July 21, 2014

S = struct('type', '.', 'subs', regexp(fieldPath, '\.', 'split'));
newObj = subsasgn(obj, S, value);

end
