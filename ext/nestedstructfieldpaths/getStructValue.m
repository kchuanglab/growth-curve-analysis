function value = getStructValue(obj, fieldPath)
%getStructValue Returns the value of the (potentially nested) field in the
%               input struct. An empty fieldPath causes return of the input
%               struct.
% obj should be a struct
% fieldPath should be a dot-delimited string indicating the full path of
%           the field
%
% Example: given foo = struct('bar', struct('baz', 'foobar')),
%          getStructValue(foo, 'bar.baz') will return 'foobar'

% This is adapted from Bruno Luong's SetProp function, first posted at
% http://www.mathworks.com/matlabcentral/newsreader/view_original/405062
% from <brunoluong@yahoo.com> at comp.soft-sys.matlab

% Ethan Li
% lietk12@gmail.com
% July 21, 2014

if ~isempty(fieldPath)
  S = struct('type', '.', 'subs', regexp(fieldPath, '\.', 'split'));
  value = subsref(obj, S);
else
  value = obj;
end

end
